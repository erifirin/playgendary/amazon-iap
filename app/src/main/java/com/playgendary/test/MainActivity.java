package com.playgendary.test;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.playgendary.amazon.iap.IapService;
import com.playgendary.amazon.iap.ProductsDataListener;
import com.playgendary.amazon.iap.ProductsDataOperationContext;
import com.playgendary.amazon.iap.PurchaseProductListener;
import com.playgendary.amazon.iap.PurchaseProductOperationContext;
import com.playgendary.amazon.iap.PurchaseUpdatesListener;
import com.playgendary.amazon.iap.PurchaseUpdatesOperationContext;
import com.playgendary.amazon.iap.data.SubscriptionRecord;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class MainActivity extends AppCompatActivity
{
    private static final String TAG = MainActivity.class.getName();
    private static final String CONSUMABLE1_PRODUCT_ID = "com.playgendary.test.consumable1";
    private static final String CONSUMABLE2_PRODUCT_ID = "com.playgendary.test.consumable2";
    private static final String NONCONSUMABLE1_PRODUCT_ID = "com.playgendary.test.nonconsumable1";
    private static final String NONCONSUMABLE2_PRODUCT_ID = "com.playgendary.test.nonconsumable2";
    private static final String SUBSCRIPTION1_PRODUCT_ID = "com.playgendary.test.subscription1";
    private static final String SUBSCRIPTION1_WEEK_PRODUCT_ID = SUBSCRIPTION1_PRODUCT_ID + ".week";
    private static final String SUBSCRIPTION1_MONTH_PRODUCT_ID = SUBSCRIPTION1_PRODUCT_ID + ".month";
    private static final String SUBSCRIPTION1_YEAR_PRODUCT_ID = SUBSCRIPTION1_PRODUCT_ID + ".year";
    private static final String SUBSCRIPTION2_PRODUCT_ID = "com.playgendary.test.subscription2";

    private static final String[] ALL_PRODUCTS_IDS = {
            CONSUMABLE1_PRODUCT_ID,
            CONSUMABLE2_PRODUCT_ID,
            NONCONSUMABLE1_PRODUCT_ID,
            NONCONSUMABLE2_PRODUCT_ID,
            SUBSCRIPTION1_WEEK_PRODUCT_ID,
            SUBSCRIPTION1_MONTH_PRODUCT_ID,
            SUBSCRIPTION1_YEAR_PRODUCT_ID,
            SUBSCRIPTION2_PRODUCT_ID
    };

    private TextView _txtNonConsumable1;
    private TextView _txtNonConsumable2;
    private TextView _txtSubscription1;
    private TextView _txtSubscription2;
    private TextView _txtLog;

    private IapService _iapService = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize In-app service
        _iapService = new IapService(this);

        _txtNonConsumable1 = findViewById(R.id.txtNonConsumable1);
        _txtNonConsumable2 = findViewById(R.id.txtNonConsumable2);
        _txtSubscription1 = findViewById(R.id.txtSubscription1);
        _txtSubscription2 = findViewById(R.id.txtSubscription2);
        _txtLog = findViewById(R.id.txtLog);
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        _iapService.onResume();
        updatePurchasesStatus();
    }

    @Override
    protected void onPause()
    {
        super.onPause();

        _iapService.onPause();
    }

    public void onFetchProductsData(@NonNull View sender)
    {
        final Set<String> productIds = new HashSet<>(Arrays.asList(ALL_PRODUCTS_IDS));
        _iapService.fetchProductsData(productIds, new ProductsDataListener() {
            @Override
            public void onProductDataResponse(@NonNull ProductsDataOperationContext context)
            {
                setLogText(context.toString());
            }
        });
    }

    public void onPurchaseConsumable1(@NonNull View sender)
    {
        _iapService.purchaseProduct(CONSUMABLE1_PRODUCT_ID, new PurchaseProductListener() {
            @Override
            public void onPurchaseResponse(@NonNull PurchaseProductOperationContext context)
            {
                setLogText(context.toString());
            }
        });
    }

    public void onPurchaseNonConsumable1(@NonNull View sender)
    {
        _iapService.purchaseProduct(NONCONSUMABLE1_PRODUCT_ID, new PurchaseProductListener() {
            @Override
            public void onPurchaseResponse(@NonNull PurchaseProductOperationContext context)
            {
                setLogText(context.toString());
                updatePurchasesStatus();
            }
        });
    }

    public void onPurchaseSubscription1(@NonNull View sender)
    {
        _iapService.purchaseProduct(SUBSCRIPTION1_WEEK_PRODUCT_ID, new PurchaseProductListener() {
            @Override
            public void onPurchaseResponse(@NonNull PurchaseProductOperationContext context)
            {
                setLogText(context.toString());
                updatePurchasesStatus();
            }
        });
    }

    public void onPurchaseSubscription2(@NonNull View sender)
    {
        _iapService.purchaseProduct(SUBSCRIPTION2_PRODUCT_ID, new PurchaseProductListener() {
            @Override
            public void onPurchaseResponse(@NonNull PurchaseProductOperationContext context)
            {
                setLogText(context.toString());
                updatePurchasesStatus();
            }
        });
    }

    public void onLogActiveSubscriptions(@NonNull View sender)
    {
        StringBuilder sb = new StringBuilder();

        Set<SubscriptionRecord> subscriptions = _iapService.getActiveSubscriptions();
        if (subscriptions != null)
            for (SubscriptionRecord rec : subscriptions)
                sb.append(rec.getProductId()).append('\n');

        setLogText(sb.toString());

        updatePurchasesStatus();
    }

    public void onRestorePurchases(@NonNull View sender)
    {
        _iapService.restorePurchase(new PurchaseUpdatesListener() {
            @Override
            public void onPurchaseUpdatesResponse(@NonNull PurchaseUpdatesOperationContext context)
            {
                setLogText(context.toString());
                updatePurchasesStatus();
            }
        });
    }

    private void setLogText(@Nullable final String text)
    {
        Log.d(TAG, text);
        _txtLog.setText(text);
    }

    private void updatePurchasesStatus()
    {
        Set<String> ids = _iapService.getPurchasedProductIds();

        _txtNonConsumable1.setText(ids != null && ids.contains(NONCONSUMABLE1_PRODUCT_ID)
                ? "Non-consumable #1 is purchased"
                : "Non-consumable #1 is NOT purchased"
        );

        _txtNonConsumable2.setText(ids != null && ids.contains(NONCONSUMABLE2_PRODUCT_ID)
                ? "Non-consumable #2 is purchased"
                : "Non-consumable #2 is NOT purchased"
        );

        _txtSubscription1.setText(ids != null && ids.contains(SUBSCRIPTION1_PRODUCT_ID)
                ? "Subscription #1 is purchased"
                : "Subscription #1 is NOT purchased"
        );

        _txtSubscription2.setText(ids != null && ids.contains(SUBSCRIPTION2_PRODUCT_ID)
                ? "Subscription #2 is purchased"
                : "Subscription #2 is NOT purchased"
        );
    }
}
