package com.lllibset.LLStoreManager;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.amazon.device.iap.model.Product;
import com.amazon.device.iap.model.Receipt;
import com.amazon.device.iap.model.UserData;
import com.lllibset.LLActivity.ILLLibSetCallback;
import com.lllibset.LLActivity.LLActivity;
import com.lllibset.LLActivity.util.ActivityListener;
import com.lllibset.LLActivity.util.LLCustomDebug;
import com.lllibset.LLActivity.util.LLLibSetCallbackProxy;
import com.playgendary.amazon.iap.IapService;
import com.playgendary.amazon.iap.ProductsDataListener;
import com.playgendary.amazon.iap.ProductsDataOperationContext;
import com.playgendary.amazon.iap.PurchaseProductListener;
import com.playgendary.amazon.iap.PurchaseProductOperationContext;
import com.playgendary.amazon.iap.PurchaseUpdatesListener;
import com.playgendary.amazon.iap.PurchaseUpdatesOperationContext;
import com.playgendary.amazon.iap.data.SubscriptionRecord;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Currency;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * Bridge to Unity class
 */
public class LLStoreManager
{
    private static final String TAG = LLStoreManager.class.getName();

    private static final String SUBS_PRODUCT_ID = "productID";
    private static final String SUBS_ORDER_ID = "orderID";
    private static final String SUBS_PURCHASE_DATE = "purchaseDate";
    private static final String SUBS_EXPIRATION_DATE = "expirationDate";

    private static IapService _iapService = null;
    private static LLLibSetCallbackProxy _receiveProductCallback = new LLLibSetCallbackProxy();
    private static LLLibSetCallbackProxy _purchaseSuccessfulCallback = new LLLibSetCallbackProxy();
    private static LLLibSetCallbackProxy _purchaseFailedCallback = new LLLibSetCallbackProxy();
    private static LLLibSetCallbackProxy _restoreCompleteCallback = new LLLibSetCallbackProxy();

    private enum LLTransactionState
    {
        Failed,
        Purchased,
        RePurchased,
        Restored
    }

    private enum LLPurchaseValidationState
    {
        Undefined,
        Valid,
        Invalid,
        EmptyInApp,
    }

    //----------------------------------------------------------------------------------------------
    //region Initialization

    /**
     * Initialize IAP service (for Unity app)
     */
    public static void LLStoreManagerRegisterCallbacks(
            @NonNull final ILLLibSetCallback receiveProductCallback,
            @NonNull final ILLLibSetCallback purchaseSuccessfulCallback,
            @NonNull final ILLLibSetCallback purchaseFailedCallback,
            @NonNull final ILLLibSetCallback restoreCompleteCallback,
            @NonNull final ILLLibSetCallback historyLoadedCallback)
    {
        // Checks whether the IAP service is already started
        if (_iapService != null)
            return;

        // initialize IAP service
        LLActivity activity = LLActivity.getSelfInstance();
        _iapService = new IapService(activity);

        // set callbacks
        _receiveProductCallback.setCallback(receiveProductCallback, LLLibSetCallbackProxy.HandlerMode.SaveThread);
        _purchaseSuccessfulCallback.setCallback(purchaseSuccessfulCallback, LLLibSetCallbackProxy.HandlerMode.SaveThread);
        _purchaseFailedCallback.setCallback(purchaseFailedCallback, LLLibSetCallbackProxy.HandlerMode.SaveThread);
        _restoreCompleteCallback.setCallback(restoreCompleteCallback, LLLibSetCallbackProxy.HandlerMode.SaveThread);

        // set activity lifecycle handler
        activity.AddHandler(new ActivityListener()
        {
            @Override
            public boolean onActivityResult(int i, int i1, Intent intent) {
                return false;
            }

            @Override
            public void onPause() {
                if (_iapService != null)
                    _iapService.onPause();
            }

            @Override
            public void onResume() {
                if (_iapService != null)
                    _iapService.onResume();
            }

            @Override
            public void onStop() {}

            @Override
            public void onDestroy() {}
        });
    }

    //endregion

    //----------------------------------------------------------------------------------------------
    //region Purchasing API

    public static void LLStoreManagerRequestProducts(@NonNull String[] productIds, int countProductIds)
    {
        Set<String> set = new HashSet<>();
        for (int i = 0; i < countProductIds; i++)
            set.add(productIds[i]);

        LLCustomDebug.logDebug(TAG, ">> RequestProducts...");

        // Request products data
        _iapService.fetchProductsData(set, new ProductsDataListener() {
            @Override
            public void onProductDataResponse(@NonNull ProductsDataOperationContext context)
            {
                // invoke callback for each product in operation context
                Map<String, Product> products = context.getProductsData();
                if (products != null)
                    for (String productId : products.keySet())
                        _receiveProductCallback.OnCallback(productId, true);

                LLCustomDebug.logDebug(TAG, "<< RequestProducts Result:" + context.toString());

                // invoke callback for each unavailable product in operation context
                Set<String> unavailableProductIds = context.getUnavailableProductIds();
                if (unavailableProductIds != null)
                    for (String productId : unavailableProductIds)
                        _receiveProductCallback.OnCallback(productId, false);
            }
        });
    }

    public static void LLStoreManagerPurchaseProduct(@NonNull final String productId, @Nullable final String rewardId)
    {
        LLCustomDebug.logDebug(TAG, ">> PurchaseProduct...");

        // process product purchasing
        _iapService.purchaseProduct(productId, new PurchaseProductListener() {
            @Override
            public void onPurchaseResponse(@NonNull PurchaseProductOperationContext context)
            {
                // prepare data for callback
                LLTransactionState transactionState = LLTransactionState.Failed;
                LLPurchaseValidationState validationState = LLPurchaseValidationState.Undefined;
                int rc = context.getResultCode();
                switch (rc)
                {
                    case ProductsDataOperationContext.RESULT_CODE_OK:
                        validationState = LLPurchaseValidationState.Valid;
                        transactionState = context.isAlreadyOwn()
                                ? LLTransactionState.RePurchased
                                : LLTransactionState.Purchased;
                        break;

                    case ProductsDataOperationContext.RESULT_CODE_VERIFICATION_FAILED:
                        validationState = LLPurchaseValidationState.Invalid;
                        transactionState = LLTransactionState.Purchased;
                        break;
                }

                LLCustomDebug.logDebug(TAG, "<< PurchaseProduct Result:" + context.toString());

                // invoke callback
                _purchaseSuccessfulCallback.OnCallback(
                        context.getProductId(),
                        rewardId != null ? rewardId : "",
                        transactionState.ordinal(),
                        validationState.ordinal());
            }
        });
    }

    public static void LLStoreManagerRestorePurchases()
    {
        LLCustomDebug.logDebug(TAG, ">> RestorePurchases...");

        // process products restoring
        _iapService.restorePurchase(new PurchaseUpdatesListener() {
            @Override
            public void onPurchaseUpdatesResponse(@NonNull PurchaseUpdatesOperationContext context)
            {
                // invoke callback for each restored purchase
                ArrayList<Receipt> receipts = context.getReceipts();
                if (receipts != null)
                    for (Receipt receipt : receipts)
                        _purchaseSuccessfulCallback.OnCallback(
                                receipt.getSku(),
                                "", // no reward Id
                                LLTransactionState.RePurchased.ordinal(),
                                LLPurchaseValidationState.Valid.ordinal());

                LLCustomDebug.logDebug(TAG, "<< RestorePurchases Result:" + context.toString());

                _restoreCompleteCallback.OnCallback(context.getResultCode() == PurchaseUpdatesOperationContext.RESULT_CODE_OK);
            }
        });
    }

    //endregion

    //----------------------------------------------------------------------------------------------
    //region Products cache utils

    public static boolean LLStoreManagerIsAvailableProduct(@Nullable final String productId)
    {
        return productId != null && _iapService.getProductsCache().containsProduct(productId);
    }

    @Nullable
    public static String LLStoreManagerLocalizedPriceProduct(@Nullable final String productId)
    {
        Product product = null;
        if (productId != null)
            product = _iapService.getProductsCache().getProduct(productId);

        return product != null ? product.getPrice() : null;
    }

    @Nullable
    public static String LLStoreManagerLocalizedTitleProduct(@Nullable final String productId)
    {
        Product product = null;
        if (productId != null)
            product = _iapService.getProductsCache().getProduct(productId);

        return product != null ? product.getTitle() : null;
    }

    @Nullable
    public static String LLStoreManagerLocalizedDescriptionProduct(@Nullable final String productId)
    {
        Product product = null;
        if (productId != null)
            product = _iapService.getProductsCache().getProduct(productId);

        return product != null ? product.getDescription() : null;
    }

    @Nullable
    public static String LLStoreManagerRealPriceProduct(@Nullable final String productId)
    {
        Product product = null;
        if (productId != null)
            product = _iapService.getProductsCache().getProduct(productId);

        if (product != null)
        {
            String price = product.getPrice();
            if (price != null)
                return price.replaceAll("[^\\d.]", "");
        }

        return null;
    }

    @Nullable
    public static String LLStoreManagerCountryCodeProduct()
    {
        UserData userData = _iapService.getCurrentUserData();
        return userData != null ? userData.getMarketplace() : null;
    }

    @Nullable
    public static String LLStoreManagerPriceCurrencyCodeProduct(@Nullable final String productId)
    {
        String countryCode = LLStoreManagerCountryCodeProduct();
        if (countryCode == null)
            return null;

        Locale locale = new Locale("EN", countryCode);
        return Currency.getInstance(locale).getCurrencyCode();
    }

    //endregion

    //----------------------------------------------------------------------------------------------
    //region Subscription utils

    @NonNull
    private static String convertSubscriptionRecordToJson(@Nullable final SubscriptionRecord subscription)
    {
        String json;
        try
        {
            JSONObject j = new JSONObject();
            j.put(SUBS_PRODUCT_ID, subscription.getProductId());
            j.put(SUBS_ORDER_ID, subscription.getReceiptId());
            j.put(SUBS_PURCHASE_DATE, subscription.getFrom() / 1000d);

            // TODO: There is no way to determine a subscription expiration date?
            long expirationDate = subscription.getFrom() + 31556952000L; // + 1 year
//            float expirationDate = calculateExpirationDateInMillis(purchase, skuDetails);
            j.put(SUBS_EXPIRATION_DATE, expirationDate / 1000d);

            json = j.toString();
        }
        catch (JSONException ignore)
        {
            json = "";
        }

        return json;
    }

    public static boolean LLStoreManagerIsPurchaseHistorySupported()
    {
        return false;
    }

    @NonNull
    public static String LLStoreManagerLastSubscription()
    {
        // getting last active subscription
        SubscriptionRecord subscription = null;
        Set<SubscriptionRecord> subscriptions = _iapService.getActiveSubscriptions();
        if (subscriptions != null && !subscriptions.isEmpty())
            for (SubscriptionRecord s : subscriptions)
            {
                if (subscription == null || s.getFrom() > subscription.getFrom())
                    subscription = s;
            }

        // check, format and return result
        return convertSubscriptionRecordToJson(subscription);
    }

    /**
     * Gets data of a subscription with specified order id
     * @param orderId Amazon receipt id
     * @return subscription data in json format or empty string if fails
     */
    @NonNull
    public static String LLStoreManagerSubscriptionForID(String orderId)
    {
        return convertSubscriptionRecordToJson(_iapService.getSubscriptionByReceiptId(orderId));
    }

    @NonNull
    public static String LLStoreManagerSubscriptionForDate(double unixTimestampDateInSeconds)
    {
        long ts = (long)(unixTimestampDateInSeconds * 1000L);

        // find active subscription
        SubscriptionRecord subscription = null;
        Set<SubscriptionRecord> subscriptions = _iapService.getActiveSubscriptions();
        if (subscriptions != null && !subscriptions.isEmpty())
            for (SubscriptionRecord s : subscriptions)
            {
                if (s.isActiveForDate(ts))
                {
                    subscription = s;
                    break;
                }
            }

        // check, format and return result
        return convertSubscriptionRecordToJson(subscription);
    }

    public static boolean LLStoreManagerIsSubscriptionTrial(String productId)
    {
        // It's impossible to check whether a subscription serves trial mode
        return false;
    }

    public static void LLStoreManagerLoadOwnedSubs()
    {
        // No need to do something here
    }

    //endregion

    //----------------------------------------------------------------------------------------------
    //region Dummy

    public static void LLStoreManagerSetPublicKey(String publicKey)
    {
    }

    public static void LLStoreManagerSetConsumableProducts(String[] productIDs, int countProductIDs)
    {
    }

    public static void LLStoreManagerSetSubscriptions(String[] subscriptionIDs, int countProductIDs)
    {
    }

    public static boolean LLStoreManagerCanMakePurchase()
    {
        return true;
    }

    public static String LLStoreManagerGetPurchaseSignature(String productId)
    {
        return "";
    }

    public static String LLStoreManagerGetPurchaseData(String productId)
    {
        return "";
    }

    public static void LLStoreManagerSetDeveloperPayload(String payload)
    {
    }

    //endregion
}
