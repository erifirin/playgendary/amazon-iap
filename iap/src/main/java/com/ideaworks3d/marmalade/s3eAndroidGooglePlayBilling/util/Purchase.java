/* Copyright (c) 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Represents an in-app billing purchase.
 */
public class Purchase
{
    private String _itemType;  // ITEM_TYPE_INAPP or ITEM_TYPE_SUBS
    private String _orderId;
    private String _sku;
    private long _purchaseTime;
    private int _purchaseState;
    private String _packageName;
    private String _developerPayload;
    private String _token;
    private String _originalJson;
    private String _signature;

    public Purchase(
            @NonNull final String itemType,
            @NonNull final String sku,
            @Nullable final String orderId,
            final long purchaseTime,
            /*final int purchaseState,*/
            @Nullable final String developerPayload)
    {
        _itemType = itemType;
        _orderId = orderId;
        _sku = sku;
        _purchaseTime = purchaseTime;
        _purchaseState = 0; //purchaseState; //The purchase state of the order. It always returns 0 (purchased).
        _originalJson = null;
        _signature = null;
        _token = null;
        _developerPayload = developerPayload;
        _packageName = null;
    }

    /** Marmalade */
    public Purchase(String token)
    {
        _token = token;
        _itemType = IabHelper.ITEM_TYPE_INAPP;
    }

    public String getItemType() { return _itemType; }
    public String getOrderId() { return _orderId; }
    public String getPackageName() { return _packageName; }
    public String getSku() { return _sku; }
    public long getPurchaseTime() { return _purchaseTime; }
    public int getPurchaseState() { return _purchaseState; }
    public String getDeveloperPayload() { return _developerPayload; }
    public String getToken() { return _token; }
    public String getOriginalJson() { return _originalJson; }
    public String getSignature() { return _signature; }

    @Override
    public String toString() { return "PurchaseInfo(type:" + _itemType + "):" + _sku; }
}
