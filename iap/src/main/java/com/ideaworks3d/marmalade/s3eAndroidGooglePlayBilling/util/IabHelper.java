package com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.amazon.device.iap.model.Product;
import com.amazon.device.iap.model.ProductType;
import com.amazon.device.iap.model.Receipt;
import com.playgendary.amazon.iap.IapService;
import com.playgendary.amazon.iap.OperationContext;
import com.playgendary.amazon.iap.ProductsDataListener;
import com.playgendary.amazon.iap.ProductsDataOperationContext;
import com.playgendary.amazon.iap.PurchaseProductListener;
import com.playgendary.amazon.iap.PurchaseProductOperationContext;
import com.playgendary.amazon.iap.data.EntitlementRecord;
import com.playgendary.amazon.iap.data.SubscriptionRecord;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class IabHelper
{
    // Billing response codes
    public static final int BILLING_RESPONSE_RESULT_OK = 0;
    public static final int BILLING_RESPONSE_RESULT_USER_CANCELED = 1;
    public static final int BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE = 3;
    public static final int BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE = 4;
    public static final int BILLING_RESPONSE_RESULT_DEVELOPER_ERROR = 5;
    public static final int BILLING_RESPONSE_RESULT_ERROR = 6;
    public static final int BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED = 7;
    public static final int BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED = 8;

    // IAB Helper error codes
    public static final int IABHELPER_ERROR_BASE = -1000;
    public static final int IABHELPER_REMOTE_EXCEPTION = -1001;
    public static final int IABHELPER_BAD_RESPONSE = -1002;
    public static final int IABHELPER_VERIFICATION_FAILED = -1003;
    public static final int IABHELPER_SEND_INTENT_FAILED = -1004;
    public static final int IABHELPER_USER_CANCELLED = -1005;
    public static final int IABHELPER_UNKNOWN_PURCHASE_RESPONSE = -1006;
    public static final int IABHELPER_MISSING_TOKEN = -1007;
    public static final int IABHELPER_UNKNOWN_ERROR = -1008;
    public static final int IABHELPER_SUBSCRIPTIONS_NOT_AVAILABLE = -1009;
    public static final int IABHELPER_INVALID_CONSUMPTION = -1010;

    // Keys for the responses from InAppBillingService
    public static final String RESPONSE_CODE = "RESPONSE_CODE";
    public static final String RESPONSE_GET_SKU_DETAILS_LIST = "DETAILS_LIST";
    public static final String RESPONSE_BUY_INTENT = "BUY_INTENT";
    public static final String RESPONSE_INAPP_PURCHASE_DATA = "INAPP_PURCHASE_DATA";
    public static final String RESPONSE_INAPP_SIGNATURE = "INAPP_DATA_SIGNATURE";
    public static final String RESPONSE_INAPP_ITEM_LIST = "INAPP_PURCHASE_ITEM_LIST";
    public static final String RESPONSE_INAPP_PURCHASE_DATA_LIST = "INAPP_PURCHASE_DATA_LIST";
    public static final String RESPONSE_INAPP_SIGNATURE_LIST = "INAPP_DATA_SIGNATURE_LIST";
    public static final String INAPP_CONTINUATION_TOKEN = "INAPP_CONTINUATION_TOKEN";

    // Item types
    public static final String ITEM_TYPE_INAPP = "inapp";
    public static final String ITEM_TYPE_SUBS = "subs";

    // some fields on the getSkuDetails response bundle
    public static final String GET_SKU_DETAILS_ITEM_LIST = "ITEM_ID_LIST";
    public static final String GET_SKU_DETAILS_ITEM_TYPE_LIST = "ITEM_TYPE_LIST";

    private static final String[] RESPONSE_CODE_DESCRIPTIONS = {
            "0:OK",
            "1:User Canceled",
            "2:Unknown",
            "3:Billing Unavailable",
            "4:Item unavailable",
            "5:Developer Error",
            "6:Error",
            "7:Item Already Owned",
            "8:Item not owned"
    };

    private static final String[] ERROR_CODE_DESCRIPTIONS = {
            "0:OK",
            "-1001:Remote exception during initialization",
            "-1002:Bad response received",
            "-1003:Purchase signature verification failed",
            "-1004:Send intent failed",
            "-1005:User cancelled",
            "-1006:Unknown purchase response",
            "-1007:Missing token",
            "-1008:Unknown error",
            "-1009:Subscriptions not available",
            "-1010:Invalid consumption attempt"
    };

    private boolean _enableDebugLog = false;
    private String _debugLogTag = null;
    private Context _context = null;
    private IapService _iapService = null;

    public IabHelper(Context ctx, String base64PublicKey)
    {
        enableDebugLogging(false, this.getClass().getName());
        _context = ctx;
    }

    //----------------------------------------------------------------------------------------------
    //region Logging

    /**
     * Enables or disable debug logging through LogCat.
     */
    public void enableDebugLogging(boolean enable, @Nullable String tag)
    {
        _enableDebugLog = enable;

        if (tag != null)
            _debugLogTag = tag;
    }

    public void enableDebugLogging(boolean enable)
    {
        enableDebugLogging(enable, null);
    }

    private void logDebug(@NonNull String msg)
    {
        if (_enableDebugLog)
            Log.d(_debugLogTag, msg);
    }

    void logError(@NonNull String msg)
    {
        Log.e(_debugLogTag, "Amazon IAP error: " + msg);
    }

    void logWarn(@NonNull String msg)
    {
        Log.w(_debugLogTag, "Amazon IAP warning: " + msg);
    }

    //endregion

    //----------------------------------------------------------------------------------------------
    //region Misc

    /**
     * Returns a human-readable description for the given response code.
     *
     * @param code The response code
     * @return A human-readable string explaining the result code.
     *     It also includes the result code numerically.
     */
    @NonNull
    public static String getResponseDesc(int code)
    {
        if (code <= IABHELPER_ERROR_BASE)
        {
            int index = IABHELPER_ERROR_BASE - code;
            if (index >= 0 && index < ERROR_CODE_DESCRIPTIONS.length)
                return ERROR_CODE_DESCRIPTIONS[index];
            else
                return String.valueOf(code) + ":Unknown IAB Helper Error";
        }
        else if (code < 0 || code >= RESPONSE_CODE_DESCRIPTIONS.length)
            return String.valueOf(code) + ":Unknown";
        else
            return RESPONSE_CODE_DESCRIPTIONS[code];
    }

    /**
     * Returns whether subscriptions are supported.
     */
    public boolean subscriptionsSupported()
    {
        return true;
    }

    /**
     * Checks that setup was done; if not, throws an exception.
     * @param operation
     */
    void checkSetupDone(String operation)
    {
        if (_iapService == null)
        {
            logError("Illegal state for operation (" + operation + "): Amazon IAP service is not set up.");
            throw new IllegalStateException("Amazon IAP service is not set up. Can't perform operation: " + operation);
        }
    }

    //endregion

    //----------------------------------------------------------------------------------------------
    //region Convertions

    @NonNull
    private static String convertProductTypeToItemType(@Nullable final ProductType productType)
    {
        return productType == ProductType.SUBSCRIPTION
                ? ITEM_TYPE_SUBS
                : ITEM_TYPE_INAPP;
    }

    /**
     * Converts operation context result code to IabHelper result code
     * @return
     */
    private static int getIabHelperResultCode(@NonNull final OperationContext context)
    {
        int resultCode = BILLING_RESPONSE_RESULT_DEVELOPER_ERROR;
        switch (context.getResultCode())
        {
            case OperationContext.RESULT_CODE_OK:
                resultCode = BILLING_RESPONSE_RESULT_OK;
                if (context instanceof PurchaseProductOperationContext &&
                    ((PurchaseProductOperationContext)context).isAlreadyOwn())
                    resultCode = BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED;
                break;

            case PurchaseProductOperationContext.RESULT_CODE_VERIFICATION_FAILED:
                resultCode = IABHELPER_VERIFICATION_FAILED;
                break;

            case PurchaseProductOperationContext.RESULT_CODE_FAILED:
            case PurchaseProductOperationContext.RESULT_CODE_BUSY:
                resultCode = BILLING_RESPONSE_RESULT_ERROR;
                break;
        }

        return resultCode;
    }

    @Nullable
    private static SkuDetails convertProductToSkuDetails(@Nullable final Product product)
    {
        if (product == null)
            return null;

        return new SkuDetails(
                convertProductTypeToItemType(product.getProductType()),
                product.getSku(),
                product.getPrice(),
                product.getTitle(),
                product.getDescription(),
                // amazon api doesn't provide detailed information about subscriptions
                "",
                "");
    }

    //endregion

    //----------------------------------------------------------------------------------------------
    //region Initialization

    /**
     * Callback for setup process. This listener's {@link #onIabSetupFinished} method is called
     * when the setup process is complete.
     */
    public interface OnIabSetupFinishedListener
    {
        /**
         * Called to notify that setup is complete.
         *
         * @param result The result of the setup process.
         */
        void onIabSetupFinished(@NonNull IabResult result);
    }

    /**
     * Starts the setup process. This will start up the setup process asynchronously.
     * You will be notified through the listener when the setup process is complete.
     * This method is safe to call from a UI thread.
     *
     * @param listener The listener to notify when the setup process is complete.
     */
    public void startSetup(@Nullable final OnIabSetupFinishedListener listener)
    {
        // If already set up, can't do it again.
        if (_iapService != null)
            throw new IllegalStateException("IAP service is already set up.");

        // Connection to IAB service
        logDebug("Starting Amazon in-app purchase service.");
        _iapService = new IapService(_context);

        // TODO: monitor activity lifecycle
        _iapService.onResume();

        // invoke callback
        if (listener != null)
            listener.onIabSetupFinished(new IabResult(BILLING_RESPONSE_RESULT_OK, "Setup successful."));
    }

    /**
     * Dispose of object, releasing resources. It's very important to call this
     * method when you are done with this object. It will release any resources
     * used by it such as service connections. Naturally, once the object is
     * disposed of, it can't be used again.
     */
    public void dispose()
    {
        logDebug("Disposing.");

        // not supported on amazon
    }

    //endregion

    //----------------------------------------------------------------------------------------------
    //region Fetch owned items

    /**
     * Listener that notifies when an inventory query operation completes.
     */
    public interface QueryInventoryFinishedListener
    {
        /**
         * Called to notify that an inventory query operation completed.
         *
         * @param result The result of the operation.
         * @param inv The inventory.
         */
        void onQueryInventoryFinished(@NonNull IabResult result, @NonNull Inventory inv, int context);
    }

    /**
     * Queries the inventory. This will query all owned items from the server, as well as
     * information on additional skus, if specified. This method may block or take long to execute.
     *
     * @param querySkuDetails if true, SKU details (price, description, etc) will be queried as well
     *     as purchase information.
     * @param moreItemSkus additional PRODUCT skus to query information on, regardless of ownership.
     *     Ignored if null or if querySkuDetails is false.
     * @param moreSubsSkus additional SUBSCRIPTIONS skus to query information on, regardless of ownership.
     *     Ignored if null or if querySkuDetails is false.
     */
    @NonNull
    public Inventory queryInventory(
            final boolean querySkuDetails,
            @Nullable final List<String> moreItemSkus,
            @Nullable final List<String> moreSubsSkus)
    {
        checkSetupDone("queryInventory");

        Inventory inventory = new Inventory();

        // add non-consumable to inventory
        Set<EntitlementRecord> entitlementRecords = _iapService.getActiveNonConsumableProducts();
        if (entitlementRecords != null)
            for (EntitlementRecord rec : entitlementRecords)
                inventory.addPurchase(new Purchase(
                        ITEM_TYPE_INAPP,
                        rec.getProductId(),
                        rec.getReceiptId(),
                        rec.getPurchaseDate(),
                        null));

        // add subscriptions to inventory
        Set<SubscriptionRecord> subscriptionRecords = _iapService.getActiveSubscriptions();
        if (subscriptionRecords != null)
            for (SubscriptionRecord rec : subscriptionRecords)
                inventory.addPurchase(new Purchase(
                        ITEM_TYPE_SUBS,
                        rec.getProductId(),
                        rec.getReceiptId(),
                        rec.getFrom(),
                        null));

        // fetch products data to fill internal cache
        if (querySkuDetails)
        {
            Set<String> productsSet = new HashSet<>();
            if (moreItemSkus != null && !moreItemSkus.isEmpty())
                productsSet.addAll(moreItemSkus);
            if (moreSubsSkus != null && !moreSubsSkus.isEmpty())
                productsSet.addAll(moreSubsSkus);

            if (!productsSet.isEmpty())
                _iapService.fetchProductsData(productsSet, null);
        }

        return inventory;
    }

    @NonNull
    public Inventory queryInventory(
            final boolean querySkuDetails,
            @Nullable final List<String> moreSkus)
    {
        return queryInventory(querySkuDetails, moreSkus, null);
    }

    /**
     * Asynchronous wrapper for inventory query. This will perform an inventory
     * query as described in {@link #queryInventory}, but will do so asynchronously
     * and call back the specified listener upon completion. This method is safe to
     * call from a UI thread.
     *
     * @param querySkuDetails as in {@link #queryInventory}
     * @param moreSkus as in {@link #queryInventory}
     * @param listener The listener to notify when the refresh operation completes.
     */
    public void queryInventoryAsync(
            final boolean querySkuDetails,
            @Nullable final List<String> moreSkus,
            final int context,
            @Nullable final QueryInventoryFinishedListener listener)
    {
        checkSetupDone("queryInventory");

        // no need async implementation here on Amazon devices
        Inventory inventory = queryInventory(querySkuDetails, moreSkus, null);

        // invoke callback
        if (listener != null)
            listener.onQueryInventoryFinished(
                    new IabResult(BILLING_RESPONSE_RESULT_OK, "Inventory refresh successful."),
                    inventory,
                    context);
    }

    public void queryInventoryAsync(@Nullable final QueryInventoryFinishedListener listener)
    {
        queryInventoryAsync(true, null, -1, listener);
    }

    public void queryInventoryAsync(
            final boolean querySkuDetails,
            final int context,
            @Nullable final QueryInventoryFinishedListener listener)
    {
        queryInventoryAsync(querySkuDetails, null, context, listener);
    }

    //endregion

    //----------------------------------------------------------------------------------------------
    //region Fetch products data

    /**
     * Marmalade specific modifications
     */

    /**
     * Listener that notifies when an available products query operation completes.
     */
    public interface QueryProductsFinishedListener
    {
        /**
         * Called to notify that an Product list query operation completed.
         *
         * @param result The result of the operation.
         * @param inv The inventory.
         */
        void onQuerySkusFinished(@NonNull IabResult result, @NonNull Inventory inv, int context);
    }

    /**
     * Asynchronous wrapper for query skus. This will query skus, but will do so asynchronously
     * and call back the specified listener upon completion. This method is safe to
     * call from a UI thread.
     *
     * @param querySkuDetails as in {@link #queryInventory}
     * @param moreSkus as in {@link #queryInventory}
     * @param listener The listener to notify when the refresh operation completes.
     */
    public void querySkusAsync(
            @Nullable final List<String> inAppSkus,
            @Nullable final List<String> subSkus,
            @Nullable final QueryProductsFinishedListener listener,
            final int context)
    {
        checkSetupDone("queryskus");

        // prepare operation arguments
        final Set<String> productsSet = new HashSet<>();
        if (inAppSkus != null && !inAppSkus.isEmpty())
            productsSet.addAll(inAppSkus);
        if (subSkus != null && !subSkus.isEmpty())
            productsSet.addAll(subSkus);

        // fetch products data
        _iapService.fetchProductsData(productsSet, new ProductsDataListener() {
            @Override
            public void onProductDataResponse(@NonNull ProductsDataOperationContext opContext)
            {
                // do nothing if listener is missing
                if (listener == null)
                    return;

                // collect products data and save to inventory
                Inventory inventory = new Inventory();
                Map<String, Product> productsData = opContext.getProductsData();
                if (productsData != null)
                    for (String productId : productsData.keySet())
                    {
                        Product product = productsData.get(productId);
                        if (product != null)
                            inventory.addSkuDetails(convertProductToSkuDetails(product));
                    }

                // invoke callback
                listener.onQuerySkusFinished(
                        new IabResult(getIabHelperResultCode(opContext), "Sku details retrieved successfully."),
                        inventory,
                        context);
            }
        });
    }

    /**
     * This will query for product info on the provided skus.
     * This method may block or take long to execute.
     * Do not call from a UI thread. For that, use the non-blocking version
     */
    @NonNull
    public Inventory querySkus(
            @Nullable final List<String> inAppSkus,
            @Nullable final List<String> subSkus)
    {
        checkSetupDone("queryProducts");

        // No way to fetch products data synchronously on amazon.
        // Using products cache instead.
        Map<String, Product> productsData = _iapService.getProductsCache().toMap();
        Inventory inventory = new Inventory();

        // collect non-consumable
        if (inAppSkus != null && !inAppSkus.isEmpty())
            for (String productId : inAppSkus)
            {
                Product product = productsData.get(productId);
                if (product != null)
                    inventory.addSkuDetails(convertProductToSkuDetails(product));
            }

        // collect subscriptions
        if (subSkus != null && !subSkus.isEmpty())
            for (String productId : subSkus)
            {
                Product product = productsData.get(productId);
                if (product != null)
                    inventory.addSkuDetails(convertProductToSkuDetails(product));
            }

        return inventory;
    }

    //endregion

    //----------------------------------------------------------------------------------------------
    //region Purchase

    /**
     * Callback that notifies when a purchase is finished.
     */
    public interface OnIabPurchaseFinishedListener
    {
        /**
         * Called to notify that an in-app purchase finished. If the purchase was successful,
         * then the sku parameter specifies which item was purchased. If the purchase failed,
         * the sku and extraData parameters may or may not be null, depending on how far the purchase
         * process went.
         *
         * @param result The result of the purchase.
         * @param info The purchase information (null if purchase failed)
         */
        void onIabPurchaseFinished(@NonNull IabResult result, @Nullable Purchase info);
    }

    /**
     * Initiate the UI flow for an in-app purchase. Call this method to initiate an in-app purchase,
     * which will involve bringing up the Amazon screen.
     *
     * @param act The calling activity.
     * @param sku The sku of the item to purchase.
     * @param itemType indicates if it's a product or a subscription (ITEM_TYPE_INAPP or ITEM_TYPE_SUBS)
     * @param requestCode A request code (to differentiate from other responses --
     *     as in {@link android.app.Activity#startActivityForResult}).
     * @param listener The listener to notify when the purchase process finishes
     * @param extraData Extra data (developer payload), which will be returned with the purchase data
     *     when the purchase completes. This extra data will be permanently bound to that purchase
     *     and will always be returned when the purchase is queried.
     */
    public void launchPurchaseFlow(
            @Nullable final Activity act,
            @Nullable final String sku,
            @Nullable final String itemType,
            final int requestCode,
            @Nullable final OnIabPurchaseFinishedListener listener,
            @Nullable final String extraData)
    {
        checkSetupDone("launchPurchaseFlow");

        // check productId has valid value
        if (sku == null || sku.isEmpty())
        {
            if (listener != null)
                listener.onIabPurchaseFinished(
                        new IabResult(IABHELPER_UNKNOWN_ERROR, "argument sku cannot be null or empty"),
                        null);
            return;
        }

        // purchase product
        _iapService.purchaseProduct(sku, new PurchaseProductListener() {
            @Override
            public void onPurchaseResponse(@NonNull PurchaseProductOperationContext context)
            {
                if (listener == null)
                    return;

                // prepare result object
                IabResult result;
                Purchase purchase = null;
                Receipt receipt = context.getReceipt();
                if (receipt != null)
                {
                    result = new IabResult(getIabHelperResultCode(context), null);
                    purchase = new Purchase(
                            convertProductTypeToItemType(receipt.getProductType()),
                            receipt.getSku(),
                            receipt.getReceiptId(),
                            receipt.getPurchaseDate().getTime(),
                            extraData);
                }
                else
                    result = new IabResult(IABHELPER_BAD_RESPONSE, "Missing receipt data");

                // invoke callback
                listener.onIabPurchaseFinished(result, purchase);
            }
        });
    }

    public void launchPurchaseFlow(
            @NonNull Activity act,
            @NonNull String sku,
            int requestCode,
            OnIabPurchaseFinishedListener listener)
    {
        launchPurchaseFlow(act, sku, requestCode, listener, "");
    }

    public void launchPurchaseFlow(
            @NonNull Activity act,
            @NonNull String sku,
            int requestCode,
            OnIabPurchaseFinishedListener listener,
            String extraData)
    {
        launchPurchaseFlow(act, sku, ITEM_TYPE_INAPP, requestCode, listener, extraData);
    }

    public void launchSubscriptionPurchaseFlow(
            @NonNull Activity act,
            @NonNull String sku,
            int requestCode,
            OnIabPurchaseFinishedListener listener)
    {
        launchSubscriptionPurchaseFlow(act, sku, requestCode, listener, "");
    }

    public void launchSubscriptionPurchaseFlow(
            @NonNull Activity act,
            @NonNull String sku,
            int requestCode,
            OnIabPurchaseFinishedListener listener,
            String extraData)
    {
        launchPurchaseFlow(act, sku, ITEM_TYPE_SUBS, requestCode, listener, extraData);
    }

    //endregion

    //----------------------------------------------------------------------------------------------
    //region Consume

    /**
     * Callback that notifies when a consumption operation finishes.
     */
    public interface OnConsumeFinishedListener
    {
        /**
         * Called to notify that a consumption has finished.
         *
         * @param purchase The purchase that was (or was to be) consumed.
         * @param result The result of the consumption operation.
         */
        void onConsumeFinished(Purchase purchase, IabResult result);
    }

    /**
     * Callback that notifies when a multi-item consumption operation finishes.
     */
    public interface OnConsumeMultiFinishedListener
    {
        /**
         * Called to notify that a consumption of multiple items has finished.
         *
         * @param purchases The purchases that were (or were to be) consumed.
         * @param results The results of each consumption operation, corresponding to each
         *     sku.
         */
        void onConsumeMultiFinished(List<Purchase> purchases, List<IabResult> results);
    }

    /**
     * Asynchronous wrapper to item consumption. Works like {@link #consume}, but
     * performs the consumption in the background and notifies completion through
     * the provided listener. This method is safe to call from a UI thread.
     *
     * @param purchase The purchase to be consumed.
     * @param listener The listener to notify when the consumption operation finishes.
     */
    public void consumeAsync(
            @NonNull final Purchase purchase,
            @Nullable final OnConsumeFinishedListener listener)
    {
        checkSetupDone("consume");
        List<Purchase> purchases = new ArrayList<>();
        purchases.add(purchase);
        consumeAsyncInternal(purchases, listener, null);
    }

    /**
     * Same as {@link consumeAsync}, but for multiple items at once.
     * @param purchases The list of PurchaseInfo objects representing the purchases to consume.
     * @param listener The listener to notify when the consumption operation finishes.
     */
    public void consumeAsync(
            @NonNull final List<Purchase> purchases,
            @Nullable final OnConsumeMultiFinishedListener listener)
    {
        checkSetupDone("consume");
        consumeAsyncInternal(purchases, null, listener);
    }

    private void consumeAsyncInternal(
            @NonNull final List<Purchase> purchases,
            @Nullable final OnConsumeFinishedListener singleListener,
            @Nullable final OnConsumeMultiFinishedListener multiListener)
    {
        // nothing to do here on amazon device

        // simulate operation result
        final List<IabResult> results = new ArrayList<>();
        for (Purchase purchase : purchases)
            results.add(new IabResult(
                    BILLING_RESPONSE_RESULT_OK,
                    "Successful consume of sku " + purchase.getSku()));

        // invoke callbacks
        if (singleListener != null)
            singleListener.onConsumeFinished(purchases.get(0), results.get(0));

        if (multiListener != null)
            multiListener.onConsumeMultiFinished(purchases, results);
    }

    //endregion
}
