/* Copyright (c) 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ideaworks3d.marmalade.s3eAndroidGooglePlayBilling.util;

import android.support.annotation.NonNull;

import java.util.Calendar;
import java.util.Locale;

/**
 * Represents an in-app product's listing details.
 */
public class SkuDetails
{
    private final String _itemType;
    private final String _sku;
    private final String _price;
    private final long _priceAmountMicros;
    private final String _title;
    private final String _description;
    private final String _subscriptionPeriod;
    private final String _freeTrialPeriod;

    public SkuDetails(
            @NonNull final String itemType,
            @NonNull final String sku,
            @NonNull final String price,
            @NonNull final String title,
            @NonNull final String description,
            @NonNull final String subscriptionPeriod,
            @NonNull final String freeTrialPeriod)
    {
        _itemType = itemType;
        _sku = sku;
        _price = price;
        _title = title;
        _description = description;
        _subscriptionPeriod = subscriptionPeriod;
        _freeTrialPeriod = freeTrialPeriod;

        long l;
        try
        {
            l = Long.parseLong(price) * 1000L;
        }
        catch (NumberFormatException ignore)
        {
            l = 0;
        }
        _priceAmountMicros = l;
    }

    @NonNull public String getSku() { return _sku; }
    @NonNull public String getType() { return _itemType; }
    @NonNull public String getPrice() { return _price; }
    @NonNull public String getTitle() { return _title; }
    @NonNull public String getDescription() { return _description; }
    public long getPriceAmountMicros() { return _priceAmountMicros; }
    @NonNull public long getSubscriptionPeriodMillis() { return convertPeriodToMillis(_subscriptionPeriod); }
    @NonNull public long getFreeTrialPeriodMillis() { return convertPeriodToMillis(_freeTrialPeriod); }
    //public String getOriginalJson() { return mJson; }

    @Override
    @NonNull
    public String toString()
    {
        return String.format(
                Locale.getDefault(),
                "SkuDetails: ItemType: %s, Sku: %s, Price: %s",
                _itemType, _sku, _price);
    }

    // ISO 8601. Duration format: P[n]Y[n]M[n]DT[n]H[n]M[n]S
    private long convertPeriodToMillis(@NonNull String period)
    {
        long result = 0L;
        if (!period.isEmpty())
        {
            period = period.substring(1);
            Calendar calendar = Calendar.getInstance();
            long millisNow = calendar.getTimeInMillis();
            int[] nextIndexAndNumber = new int[2];

            for (int i = 0; i < 4; ++i)
            {
                byte calendarField;
                char iso8601symbol;
                switch (i)
                {
                    case 0:
                        calendarField = 1;
                        iso8601symbol = 'Y';
                        break;
                    case 1:
                        calendarField = 2;
                        iso8601symbol = 'M';
                        break;
                    case 2:
                        calendarField = 4;
                        iso8601symbol = 'W';
                        break;
                    case 3:
                    default:
                        calendarField = 7;
                        iso8601symbol = 'D';
                }

                nextIndexAndNumber = findNextIndexAndParseNumber(period, iso8601symbol);
                calendar.add(calendarField, nextIndexAndNumber[1]);
                if (nextIndexAndNumber[0] != -1)
                {
                    period = period.substring(nextIndexAndNumber[0]);
                }
            }

            result = calendar.getTimeInMillis() - millisNow;
        }

        return result;
    }

    private int[] findNextIndexAndParseNumber(@NonNull String str, char key)
    {
        int[] result = new int[]{str.indexOf(key), 0};
        if (result[0] != -1)
        {
            result[1] = Integer.parseInt(str.substring(0, result[0]));
            ++result[0];
        }

        return result;
    }
}
