package com.playgendary.amazon.iap;

import android.support.annotation.NonNull;

public interface PurchaseProductListener
{
    void onPurchaseResponse(@NonNull PurchaseProductOperationContext context);
}

