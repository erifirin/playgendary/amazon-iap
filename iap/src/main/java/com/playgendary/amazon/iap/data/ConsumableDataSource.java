package com.playgendary.amazon.iap.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.util.Log;

/**
 * DAO class for sample purchase data
 * 
 * 
 */
public class ConsumableDataSource
{
    public enum PurchaseStatus
    {
        PAID, FULFILLED, UNAVAILABLE, UNKNOWN
    }

    private static final String TAG = ConsumableDataSource.class.getName();
    private static final String[] ALL_COLUMNS = {
            DbContext.COLUMN_RECEIPT_ID,
            DbContext.COLUMN_USER_ID,
            DbContext.COLUMN_STATUS,
    };

    private final DbContext _dbContext;

    public ConsumableDataSource(@NonNull final DbContext dbContext)
    {
        _dbContext = dbContext;
    }

    private ConsumableRecord cursorToPurchaseRecord(final Cursor cursor)
    {
        final ConsumableRecord purchaseRecord = new ConsumableRecord();
        purchaseRecord.setReceiptId(cursor.getString(cursor.getColumnIndex(DbContext.COLUMN_RECEIPT_ID)));
        purchaseRecord.setUserId(cursor.getString(cursor.getColumnIndex(DbContext.COLUMN_USER_ID)));

        try
        {
            purchaseRecord.setStatus(PurchaseStatus.valueOf(cursor.getString(cursor.getColumnIndex(DbContext.COLUMN_STATUS))));
        }
        catch (final Exception e)
        {
            purchaseRecord.setStatus(PurchaseStatus.UNKNOWN);
        }

        return purchaseRecord;
    }

    /**
     * Create the purchase record in sqlite database
     * 
     * @param receiptId
     *            amazon's receipt id
     * @param userId
     *            amazon user id
     * @param status
     *            the status for the purchase
     */
    public void createPurchase(final String receiptId, final String userId, final PurchaseStatus status)
    {
        Log.d(TAG, "createPurchase: receiptId (" + receiptId + "),userId (" + userId + "), status (" + status + ")");

        final ContentValues values = new ContentValues();
        values.put(DbContext.COLUMN_RECEIPT_ID, receiptId);
        values.put(DbContext.COLUMN_USER_ID, userId);
        values.put(DbContext.COLUMN_STATUS, status.toString());

        try
        {
            final SQLiteDatabase database = _dbContext.getWritableDatabase();
            database.insertOrThrow(DbContext.TABLE_CONSUMABLE, null, values);
        }
        catch (final SQLException e)
        {
            Log.w(TAG, "A purchase with given receipt id already exists, simply discard the new purchase record");
        }
    }

    /**
     * Return the purchase record by receipt id
     * 
     * @param receiptId
     *            amazon receipt id
     * @param userId
     *            user id used to verify the purchase record
     * @return
     */
    public final ConsumableRecord getPurchaseRecord(final String receiptId, final String userId)
    {
        Log.d(TAG, "getPurchaseRecord: receiptId (" + receiptId + "), userId (" + userId + ")");

        final SQLiteDatabase database = _dbContext.getWritableDatabase();
        final String where = DbContext.COLUMN_RECEIPT_ID + " = ?";
        final Cursor cursor = database.query(DbContext.TABLE_CONSUMABLE,
                                             ALL_COLUMNS,
                                             where,
                                             new String[] { receiptId },
                                             null,
                                             null,
                                             null);
        cursor.moveToFirst();

        // no record found for the given receipt id
        if (cursor.isAfterLast())
        {
            Log.d(TAG, "getPurchaseRecord: no record found for receipt id (" + receiptId + ")");
            cursor.close();
            return null;
        }
        final ConsumableRecord purchaseRecord = cursorToPurchaseRecord(cursor);
        cursor.close();
        if (purchaseRecord.getUserId() != null && purchaseRecord.getUserId().equalsIgnoreCase(userId))
        {
            Log.d(TAG, "getPurchaseRecord: record found for receipt id (" + receiptId + ")");
            return purchaseRecord;
        }
        else {
            Log.d(TAG, "getPurchaseRecord: user id not match, receipt id (" + receiptId + "), userId (" + userId + ")");
            // cannot verify the purchase is for the correct user;
            return null;
        }
    }

    /**
     * Update Purchase status for given receipt id
     * 
     * @param receiptId
     *            receipt id to update
     * @param fromStatus
     *            current status for the purchase record in table
     * @param toStatus
     *            latest status for the purchase record
     * @return
     */
    public boolean updatePurchaseStatus(final String receiptId,
            final PurchaseStatus fromStatus,
            final PurchaseStatus toStatus)
    {
        Log.d(TAG, "updatePurchaseStatus: receiptId (" + receiptId + "), status:(" + fromStatus + "->" + toStatus + ")");

        String where = DbContext.COLUMN_RECEIPT_ID + " = ?";
        String[] whereArgs = new String[] { receiptId };

        if (fromStatus != null) {
            where = where + " and " + DbContext.COLUMN_STATUS + " = ?";
            whereArgs = new String[] { receiptId, fromStatus.toString() };
        }
        final ContentValues values = new ContentValues();
        values.put(DbContext.COLUMN_STATUS, toStatus.toString());

        final SQLiteDatabase database = _dbContext.getWritableDatabase();
        final int updated = database.update(DbContext.TABLE_CONSUMABLE, values, where, whereArgs);
        Log.d(TAG, "updatePurchaseStatus: updated " + updated);
        return updated > 0;
    }
}
