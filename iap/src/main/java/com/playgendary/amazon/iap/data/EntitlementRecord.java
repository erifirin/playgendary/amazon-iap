package com.playgendary.amazon.iap.data;

public class EntitlementRecord
{
    public static final long DATE_NOT_SET = -1;

    private String _receiptId;
    private String _userId;
    private String _productId;
    private long _purchaseDate;
    private long _cancelDate = DATE_NOT_SET;

    public long getCancelDate()
    {
        return _cancelDate;
    }

    public void setCancelDate(final long cancelDate)
    {
        _cancelDate = cancelDate;
    }

    public String getReceiptId()
    {
        return _receiptId;
    }

    public void setReceiptId(final String receiptId)
    {
        _receiptId = receiptId;
    }

    public String getUserId()
    {
        return _userId;
    }

    public void setUserId(final String userId)
    {
        _userId = userId;
    }

    public String getProductId()
    {
        return _productId;
    }

    public void setProductId(final String productId)
    {
        _productId = productId;
    }

    public long getPurchaseDate()
    {
        return _purchaseDate;
    }

    public void setPurchaseDate(final long purchaseDate)
    {
        _purchaseDate = purchaseDate;
    }

    public boolean isActiveNow()
    {
        return _cancelDate == DATE_NOT_SET;
    }
}
