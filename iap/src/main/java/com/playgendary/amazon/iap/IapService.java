package com.playgendary.amazon.iap;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.amazon.device.iap.PurchasingListener;
import com.amazon.device.iap.PurchasingService;
import com.amazon.device.iap.model.FulfillmentResult;
import com.amazon.device.iap.model.ProductDataResponse;
import com.amazon.device.iap.model.PurchaseResponse;
import com.amazon.device.iap.model.PurchaseUpdatesResponse;
import com.amazon.device.iap.model.Receipt;
import com.amazon.device.iap.model.UserData;
import com.amazon.device.iap.model.UserDataResponse;
import com.playgendary.amazon.iap.data.ConsumableDataSource;
import com.playgendary.amazon.iap.data.DbContext;
import com.playgendary.amazon.iap.data.EntitlementRecord;
import com.playgendary.amazon.iap.data.EntitlementsDataSource;
import com.playgendary.amazon.iap.data.SubscriptionDataSource;
import com.playgendary.amazon.iap.data.SubscriptionRecord;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class IapService implements PurchasingListener
{
    public static final String TAG = IapService.class.getName();

    private final OperationsCollection _operations = new OperationsCollection();
    private UserData _userData = null;
    private final DbContext _dbContext;
    private final ProductsCache _productsCache = new ProductsCache();
    private final SubscriptionDataSource _subscriptionDataSource;
    private final EntitlementsDataSource _entitlementsDataSource;
    private final ConsumableDataSource _consumableDataSource;

    //----------------------------------------------------------------------------------------------
    //region Initialization and lifecycle

    public IapService(@NonNull final Context context)
    {
        Context applicationContext = context.getApplicationContext();

        _dbContext = new DbContext(applicationContext);
        _subscriptionDataSource = new SubscriptionDataSource(_dbContext);
        _entitlementsDataSource = new EntitlementsDataSource(_dbContext);
        _consumableDataSource = new ConsumableDataSource(_dbContext);

        PurchasingService.registerListener(applicationContext, this);
    }

    public void onResume()
    {
        _dbContext.getDatabase();
        PurchasingService.getUserData();
        fetchPurchaseUpdates(null);
    }

    public void onPause()
    {
        _dbContext.close();
    }

    //endregion

    //----------------------------------------------------------------------------------------------
    //region IAP methods

    public void fetchProductsData(@NonNull Set<String> productIds, @Nullable ProductsDataListener callback)
    {
        synchronized (_operations)
        {
            // register new async operation
            ProductsDataOperationContext ctx = new ProductsDataOperationContext(productIds, callback);

            // run
            Set<String> nextProductsIdsPage = ctx.getNextProductsIdsPage();
            ctx.setRequestId(PurchasingService.getProductData(nextProductsIdsPage));
            _operations.addAsyncOperation(ctx);
        }
    }

    private void fetchPurchaseUpdates(@Nullable PurchaseUpdatesListener callback)
    {
        synchronized (_operations)
        {
            // register new interlocked operation
            PurchaseUpdatesOperationContext ctx = new PurchaseUpdatesOperationContext(false, callback);
            if (!_operations.setInterlockedOperation(ctx))
            {
                ctx.setUserData(getCurrentUserData());
                ctx.invokeCallback(OperationContext.RESULT_CODE_BUSY);
                return;
            }

            // run
            ctx.setRequestId(PurchasingService.getPurchaseUpdates(ctx.getReset()));
        }
    }

    public void purchaseProduct(@NonNull final String productId, @Nullable PurchaseProductListener callback)
    {
        synchronized (_operations)
        {
            // register new interlocked operation
            PurchaseProductOperationContext ctx = new PurchaseProductOperationContext(productId, callback);
            if (!_operations.setInterlockedOperation(ctx))
            {
                ctx.setUserData(getCurrentUserData());
                ctx.invokeCallback(OperationContext.RESULT_CODE_BUSY);
                return;
            }

            // run
            ctx.setRequestId(PurchasingService.purchase(productId));
        }
    }

    public void restorePurchase(@Nullable PurchaseUpdatesListener callback)
    {
        synchronized (_operations)
        {
            // register new interlocked operation
            PurchaseUpdatesOperationContext ctx = new PurchaseUpdatesOperationContext(true, callback);
            if (!_operations.setInterlockedOperation(ctx))
            {
                ctx.setUserData(getCurrentUserData());
                ctx.invokeCallback(OperationContext.RESULT_CODE_BUSY);
                return;
            }

            // run
            ctx.setRequestId(PurchasingService.getPurchaseUpdates(ctx.getReset()));
        }
    }

    @Nullable
    public Set<SubscriptionRecord> getActiveSubscriptions()
    {
        final UserData userData = getCurrentUserData();
        if (userData == null)
            return null;

        // get subscriptions
        HashSet<SubscriptionRecord> purchased = new HashSet<>();
        List<SubscriptionRecord> subscriptions = _subscriptionDataSource.getSubscriptionRecords(userData.getUserId(), true);
        for (SubscriptionRecord rec : subscriptions)
            purchased.add(rec);

        return purchased;
    }

    @Nullable
    public SubscriptionRecord getSubscriptionByReceiptId(@NonNull final String receiptId)
    {
        final UserData userData = getCurrentUserData();
        if (userData == null)
            return null;

        // get subscriptions
        return _subscriptionDataSource.getSubscriptionRecordByReceiptId(userData.getUserId(), receiptId);
    }

    @Nullable
    public List<SubscriptionRecord> getSubscriptionsByProductId(@NonNull final String productId)
    {
        final UserData userData = getCurrentUserData();
        if (userData == null)
            return null;

        // get subscriptions
        return _subscriptionDataSource.getSubscriptionRecordsByProductId(userData.getUserId(), productId, false);
    }

    @Nullable
    public SubscriptionRecord getActiveSubscriptionByProductId(@NonNull final String productId)
    {
        final UserData userData = getCurrentUserData();
        if (userData == null)
            return null;

        // get subscriptions
        List<SubscriptionRecord> subscriptions = _subscriptionDataSource.getSubscriptionRecordsByProductId(userData.getUserId(), productId, true);
        return subscriptions.isEmpty() ? null : subscriptions.get(0);
    }

    @Nullable
    public Set<EntitlementRecord> getActiveNonConsumableProducts()
    {
        final UserData userData = getCurrentUserData();
        if (userData == null)
            return null;

        // get non-consumable
        HashSet<EntitlementRecord> purchased = new HashSet<>();
        List<EntitlementRecord> entitlements = _entitlementsDataSource.getEntitlementRecordsByUserId(userData.getUserId(), true);
        for (EntitlementRecord rec : entitlements)
            purchased.add(rec);

        return purchased;
    }

    @Nullable
    public Set<String> getPurchasedProductIds()
    {
        final UserData userData = getCurrentUserData();
        if (userData == null)
            return null;

        HashSet<String> purchased = new HashSet<>();

        // get subscriptions
        List<SubscriptionRecord> subscriptions = _subscriptionDataSource.getSubscriptionRecords(userData.getUserId(), true);
        for (SubscriptionRecord rec : subscriptions)
            purchased.add(rec.getProductId());

        // get non-consumable
        List<EntitlementRecord> entitlements = _entitlementsDataSource.getEntitlementRecordsByUserId(userData.getUserId(), true);
        for (EntitlementRecord rec : entitlements)
            purchased.add(rec.getProductId());

        return purchased;
    }

    //endregion

    //----------------------------------------------------------------------------------------------
    //region Misc

    @Nullable
    public UserData getCurrentUserData()
    {
        return _userData;
    }

    @NonNull
    public ProductsCache getProductsCache()
    {
        return _productsCache;
    }

    private void processPurchase(
            @Nullable final PurchaseProductOperationContext context,
            @Nullable Receipt receipt,
            @Nullable UserData userData)
    {
        // check consistency
        if (context != null)
        {
            if (receipt == null)
                receipt = context.getReceipt();

            if (userData == null)
                userData = context.getUserData();
        }

        if (userData == null || receipt == null)
        {
            Log.e(TAG, "Failed to process in-app purchase");
            if (context != null)
                context.invokeCallback(OperationContext.RESULT_CODE_INTERNAL_EXCEPTION, "Failed to process in-app purchase due invalid data");
            return;
        }

        // process purchase
        switch (receipt.getProductType())
        {
            case CONSUMABLE:
                handleConsumablePurchase(context, receipt, userData);
                break;

            case ENTITLED:
                handleEntitlementPurchase(context, receipt, userData);
                break;

            case SUBSCRIPTION:
                handleSubscriptionPurchase(context, receipt, userData);
                break;

            default:
                if (context != null)
                    context.invokeCallback(
                            OperationContext.RESULT_CODE_INTERNAL_EXCEPTION,
                            "Failed to process in-app purchase with unknown type: " + receipt.getProductType().toString());
                break;
        }
    }

    private boolean verifyReceiptFromYourService(@NonNull final String receiptId, @NonNull final UserData userData)
    {
        // TODO Add your own server side accessing and verification code
        return true;
    }

    //endregion

    //----------------------------------------------------------------------------------------------
    //region PurchasingListener implementation

    @Override
    public void onUserDataResponse(UserDataResponse response)
    {
        switch (response.getRequestStatus())
        {
            case SUCCESSFUL:
                _userData = response.getUserData();
                break;

            default:
            //case FAILED:
                _userData = null;
                Log.e(TAG, "Failed to fetch user data");
                break;
        }
    }

    @Override
    public void onProductDataResponse(ProductDataResponse response)
    {
        // get operation context
        ProductsDataOperationContext ctx = null;
        synchronized (_operations)
        {
            ctx = (ProductsDataOperationContext)_operations.removeAsyncOperation(response.getRequestId());
            if (ctx == null)
                return;
        }

        // cache products
        _productsCache.putAllProducts(response.getProductData());
        ctx.addProductsData(response.getProductData());

        // process next page of productIds
        Set<String> nextProductsIdsPage = ctx.getNextProductsIdsPage();
        if (nextProductsIdsPage != null)
        {
            // run next iteration
            ctx.setRequestId(PurchasingService.getProductData(nextProductsIdsPage));
            _operations.addAsyncOperation(ctx);
        }
        else
            // finalize operation
            ctx.invokeCallback(OperationContext.RESULT_CODE_OK, response);
    }

    @Override
    public void onPurchaseResponse(PurchaseResponse response)
    {
        PurchaseProductOperationContext ctx = null;
        synchronized (_operations)
        {
            ctx = (PurchaseProductOperationContext)_operations.releaseInterlockedOperation(response.getRequestId());
            if (ctx == null)
                return;
        }

        ctx.setPurchaseResponse(response);
        switch (response.getRequestStatus())
        {
            case SUCCESSFUL:
                processPurchase(ctx, null, null);
                break;

            case ALREADY_PURCHASED:
                ctx.invokeCallback(OperationContext.RESULT_CODE_OK);
                break;

            default:
                ctx.invokeCallback(OperationContext.RESULT_CODE_FAILED);
        }
    }

    @Override
    public void onPurchaseUpdatesResponse(PurchaseUpdatesResponse response)
    {
        // get context
        PurchaseUpdatesOperationContext ctx;
        synchronized (_operations)
        {
            ctx = (PurchaseUpdatesOperationContext)_operations.peekInterlockedOperation(response.getRequestId());
            if (ctx == null)
                return;
        }

        // Process receipts
        final PurchaseUpdatesResponse.RequestStatus status = response.getRequestStatus();
        switch (status)
        {
            case SUCCESSFUL:
                // process receipts
                for (final Receipt receipt : response.getReceipts())
                {
                    processPurchase(null, receipt, response.getUserData());
                    if (!receipt.isCanceled())
                        ctx.addReceipt(receipt);
                }
                ctx.setResultCode(OperationContext.RESULT_CODE_OK);

                // is it necessary to request next page
                if (response.hasMore())
                {
                    PurchasingService.getPurchaseUpdates(ctx.getReset());
                    return;
                }
                break;

            case FAILED:
                if (ctx.getResultCode() == OperationContext.RESULT_CODE_NONE)
                    ctx.setResultCode(OperationContext.RESULT_CODE_OK);
                break;
        }

        // finalize operation
        _operations.releaseInterlockedOperation(ctx);
        ctx.setPurchaseResponse(response);
        ctx.invokeCallback(ctx.getResultCode());
    }

    //endregion

    //----------------------------------------------------------------------------------------------
    //region Subscriptions processing

    private void handleSubscriptionPurchase(
            @Nullable final PurchaseProductOperationContext context,
            @NonNull Receipt receipt,
            @NonNull UserData userData)
    {
        try
        {
            // Check whether this receipt is for an expired or canceled subscription
            if (receipt.isCanceled())
                revokeSubscription(context, receipt, userData);
            else
            {
                // We strongly recommend that you verify the receipt on server-side.
                if (!verifyReceiptFromYourService(receipt.getReceiptId(), userData))
                {
                    // if the purchase cannot be verified,
                    // show relevant error message to the customer.
                    // showMessage("Purchase cannot be verified, please retry later.");
                    if (context != null)
                        context.invokeCallback(OperationContext.RESULT_CODE_VERIFICATION_FAILED);
                    return;
                }

                grantSubscriptionPurchase(context, receipt, userData);
            }
        }
        catch (final Throwable e)
        {
            // showMessage("Purchase cannot be completed, please retry");
            if (context != null)
                context.invokeCallback(OperationContext.RESULT_CODE_INTERNAL_EXCEPTION);
        }
    }

    private void grantSubscriptionPurchase(
            @Nullable final PurchaseProductOperationContext context,
            @NonNull Receipt receipt,
            @NonNull UserData userData)
    {
        try
        {
            // Set the purchase status to fulfilled for your application
            _subscriptionDataSource.insertOrUpdateSubscriptionRecord(
                    receipt.getReceiptId(),
                    userData.getUserId(),
                    receipt.getSku(),
                    receipt.getPurchaseDate().getTime(),
                    receipt.getCancelDate() == null
                            ? SubscriptionRecord.DATE_NOT_SET
                            : receipt.getCancelDate().getTime()
            );

            PurchasingService.notifyFulfillment(receipt.getReceiptId(), FulfillmentResult.FULFILLED);
            Log.d(TAG, "Subscription successfully purchased: " + receipt.getSku());
            if (context != null)
                context.invokeCallback(OperationContext.RESULT_CODE_OK);
        }
        catch (final Throwable e)
        {
            // If for any reason the app is not able to fulfill the purchase,
            // add your own error handling code here.
            final String msg = "Failed to grant subscription purchase, with error " + e.getMessage();
            Log.e(TAG, msg);
            if (context != null)
                context.invokeCallback(OperationContext.RESULT_CODE_INTERNAL_EXCEPTION, msg);
        }

        // TODO: if the sku is not applicable anymore, call PurchasingService.notifyFulfillment with status "UNAVAILABLE"
        // PurchasingService.notifyFulfillment(receipt.getReceiptId(), FulfillmentResult.UNAVAILABLE);
    }

    private void revokeSubscription(
            @Nullable final PurchaseProductOperationContext context,
            @NonNull Receipt receipt,
            @NonNull UserData userData)
    {
        Log.d(TAG, "Subscription cancelled: " + receipt.getSku());
        _subscriptionDataSource.cancelSubscription(
                receipt.getReceiptId(),
                receipt.getCancelDate().getTime());

        if (context != null)
            context.invokeCallback(OperationContext.RESULT_CODE_OK, "Subscription cancelled");
    }

    //endregion

    //----------------------------------------------------------------------------------------------
    //region Non-consumable processing

    private void handleEntitlementPurchase(
            @Nullable final PurchaseProductOperationContext context,
            @NonNull Receipt receipt,
            @NonNull UserData userData)
    {
        try
        {
            // Check whether this receipt is to revoke a entitlement purchase
            if (receipt.isCanceled())
                revokeEntitlement(context, receipt, userData);
            else
            {
                // We strongly recommend that you verify the receipt server-side.
                if (!verifyReceiptFromYourService(receipt.getReceiptId(), userData))
                {
                    // if the purchase cannot be verified,
                    // show relevant error message to the customer.
                    // showMessage("Purchase cannot be verified, please retry later.");
                    if (context != null)
                        context.invokeCallback(OperationContext.RESULT_CODE_VERIFICATION_FAILED);
                    return;
                }

                grantEntitlementPurchase(context, receipt, userData);
            }
        }
        catch (final Throwable e)
        {
            //showMessage("Purchase cannot be completed, please retry");
            if (context != null)
                context.invokeCallback(OperationContext.RESULT_CODE_INTERNAL_EXCEPTION);
        }
    }

    private void grantEntitlementPurchase(
            @Nullable final PurchaseProductOperationContext context,
            @NonNull Receipt receipt,
            @NonNull UserData userData)
    {
        try
        {
            // Set the purchase status to fulfilled for your application
            _entitlementsDataSource.insertOrUpdateEntitlementRecord(
                    receipt.getReceiptId(),
                    userData.getUserId(),
                    receipt.getSku(),
                    receipt.getPurchaseDate() != null
                            ? receipt.getPurchaseDate().getTime()
                            : EntitlementRecord.DATE_NOT_SET,
                    receipt.isCanceled()
                            ? receipt.getCancelDate().getTime()
                            : EntitlementRecord.DATE_NOT_SET
            );

            PurchasingService.notifyFulfillment(receipt.getReceiptId(), FulfillmentResult.FULFILLED);
            Log.d(TAG, "Entitlement successfully purchased: " + receipt.getSku());
            if (context != null)
                context.invokeCallback(OperationContext.RESULT_CODE_OK);
        }
        catch (final Throwable e)
        {
            // If for any reason the app is not able to fulfill the purchase,
            // add your own error handling code here.
            final String msg = "Failed to grant entitlement purchase, with error " + e.getMessage();
            Log.e(TAG, msg);
            if (context != null)
                context.invokeCallback(OperationContext.RESULT_CODE_INTERNAL_EXCEPTION, msg);
        }
    }

    private void revokeEntitlement(
            @Nullable final PurchaseProductOperationContext context,
            @NonNull Receipt receipt,
            @NonNull UserData userData)
    {
        Log.d(TAG, "Entitlement cancelled: " + receipt.getSku());

        String receiptId = receipt.getReceiptId();
        final EntitlementRecord record;
        if (receiptId == null)
        {
            // The revoked receipt's receipt id may be null on older devices.
            record = _entitlementsDataSource.getLatestEntitlementRecordBySku(userData.getUserId(), receipt.getSku());
            receiptId = record.getReceiptId();
        }
        else
            record = _entitlementsDataSource.getEntitlementRecordByReceiptId(receiptId);

        // Cancel entitlement
        if (record != null)
        {
            if (record.getCancelDate() == EntitlementRecord.DATE_NOT_SET || record.getCancelDate() > System.currentTimeMillis())
                _entitlementsDataSource.cancelEntitlement(
                        receiptId,
                        receipt.getCancelDate() != null
                                ? receipt.getCancelDate().getTime()
                                : System.currentTimeMillis()
                );
        }

        // invoke callback
        if (context != null)
            context.invokeCallback(OperationContext.RESULT_CODE_OK, "Entitlement cancelled");
    }

    //endregion

    //----------------------------------------------------------------------------------------------
    //region Consumable processing

    private void handleConsumablePurchase(
            @Nullable final PurchaseProductOperationContext context,
            @NonNull Receipt receipt,
            @NonNull UserData userData)
    {
        try
        {
            // Check whether this receipt is to revoke a entitlement purchase
            if (receipt.isCanceled())
                revokeConsumable(context, receipt, userData);
            else
            {
                // We strongly recommend that you verify the receipt server-side.
                if (!verifyReceiptFromYourService(receipt.getReceiptId(), userData))
                {
                    // if the purchase cannot be verified,
                    // show relevant error message to the customer.
                    // showMessage("Purchase cannot be verified, please retry later.");
                    if (context != null)
                        context.invokeCallback(OperationContext.RESULT_CODE_VERIFICATION_FAILED);
                    return;
                }

                grantConsumablePurchase(context, receipt, userData);
            }
        }
        catch (final Throwable e)
        {
            //showMessage("Purchase cannot be completed, please retry");
            if (context != null)
                context.invokeCallback(OperationContext.RESULT_CODE_INTERNAL_EXCEPTION);
        }
    }

    private void grantConsumablePurchase(
            @Nullable final PurchaseProductOperationContext context,
            @NonNull Receipt receipt,
            @NonNull UserData userData)
    {
        PurchasingService.notifyFulfillment(receipt.getReceiptId(), FulfillmentResult.FULFILLED);
        Log.d(TAG, "Consumable successfully purchased: " + receipt.getSku());

        if (context != null)
            context.invokeCallback(OperationContext.RESULT_CODE_OK);
    }

    private void revokeConsumable(
            @Nullable final PurchaseProductOperationContext context,
            @NonNull Receipt receipt,
            @NonNull UserData userData)
    {
        Log.d(TAG, "Consumable cancelled: " + receipt.getSku());

        if (context != null)
            context.invokeCallback(OperationContext.RESULT_CODE_OK);
    }

    //endregion
}



