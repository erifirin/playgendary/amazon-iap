package com.playgendary.amazon.iap;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.amazon.device.iap.model.PurchaseUpdatesResponse;
import com.amazon.device.iap.model.Receipt;

import java.util.ArrayList;

public class PurchaseUpdatesOperationContext extends OperationContext
{
    private boolean _reset;
    private @Nullable PurchaseUpdatesListener _callback;
    private ArrayList<Receipt> _receipts = new ArrayList<>();
    private PurchaseUpdatesResponse.RequestStatus _requestStatus;

    PurchaseUpdatesOperationContext(boolean reset, @Nullable final PurchaseUpdatesListener callback)
    {
        _reset = reset;
        _callback = callback;
    }

    boolean getReset()
    {
        return _reset;
    }

    void setPurchaseResponse(@NonNull final PurchaseUpdatesResponse response)
    {
        setUserData(response.getUserData());
        _requestStatus = response.getRequestStatus();
    }

    void addReceipt(@NonNull Receipt receipt)
    {
        _receipts.add(receipt);
    }

    @Nullable
    public ArrayList<Receipt> getReceipts()
    {
        return _receipts;
    }

    void invokeCallback(int resultCode)
    {
        invokeCallback(resultCode, null);
    }

    void invokeCallback(int resultCode, @Nullable final String resultMessage)
    {
        setResultCode(resultCode);
        setResultMessage(resultMessage);

        if (_callback != null)
        {
            PurchaseUpdatesListener callback = _callback;
            _callback = null;
            callback.onPurchaseUpdatesResponse(this);
        }
    }

    @NonNull
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append(", purchaseUpdatesRequestStatus: ").append(_requestStatus != null ? _requestStatus.toString() : "null");
        for (Receipt receipt : _receipts)
            sb.append(receipt.toString()).append('\n');

        return sb.toString();
    }
}
