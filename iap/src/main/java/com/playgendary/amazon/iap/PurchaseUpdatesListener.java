package com.playgendary.amazon.iap;

import android.support.annotation.NonNull;

public interface PurchaseUpdatesListener
{
    void onPurchaseUpdatesResponse(@NonNull PurchaseUpdatesOperationContext context);
}

