package com.playgendary.amazon.iap;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.amazon.device.iap.model.RequestId;
import com.amazon.device.iap.model.UserData;

import java.util.Locale;

public class OperationContext
{
    public static final int RESULT_CODE_NONE = 0;
    public static final int RESULT_CODE_OK = 1;
    public static final int RESULT_CODE_FAILED = 2;
    public static final int RESULT_CODE_BUSY = 3;

    public static final int RESULT_CODE_VERIFICATION_FAILED = 4;

    public static final int RESULT_CODE_INTERNAL_EXCEPTION = -101;

    private RequestId _requestId;
    private UserData _userData;
    private int _resultCode;
    private String _resultMessage;

    public int getResultCode()
    {
        return _resultCode;
    }

    void setResultCode(int resultCode)
    {
        _resultCode = resultCode;
    }

    @Nullable
    public String getResultMessage()
    {
        return _resultMessage;
    }

    void setResultMessage(@Nullable final String resultMessage)
    {
        _resultMessage = resultMessage;
    }

    @Nullable
    public RequestId getRequestId()
    {
        return _requestId;
    }

    void setRequestId(@NonNull final RequestId requestId)
    {
//        if (_requestId != null)
//            throw new IllegalStateException("RequestId is already assigned with this operation context");
        _requestId = requestId;
    }

    @Nullable
    public UserData getUserData()
    {
        return _userData;
    }

    void setUserData(@Nullable final UserData userData)
    {
        _userData = userData;
    }


    @NonNull
    @Override
    public String toString()
    {
        return String.format(Locale.getDefault(), "resultCode: %d, requestId: \"%s\", userData: \"%s\"",
                _resultCode,
                _requestId != null ? _requestId.toString() : "null",
                _userData != null ? _userData.toString() : "null");
    }
}
