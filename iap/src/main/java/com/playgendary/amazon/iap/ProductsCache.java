package com.playgendary.amazon.iap;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.amazon.device.iap.model.Product;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ProductsCache
{
    private Map<String, Product> _collection = new HashMap<>();

    public void putProduct(@NonNull final Product product)
    {
        _collection.put(product.getSku(), product);
    }

    public void putAllProducts(@NonNull final Map<String, Product> products)
    {
        _collection.putAll(products);
    }

    public void putAllProducts(@NonNull final Set<Product> products)
    {
        for (Product product : products)
            _collection.put(product.getSku(), product);
    }

    @Nullable
    public Product getProduct(@NonNull final String productId)
    {
        return _collection.get(productId);
    }

    @NonNull
    public Set<Product> toSet()
    {
        HashSet<Product> set = new HashSet<>(_collection.size());
        for (String productId : _collection.keySet())
            set.add(_collection.get(productId));
        return set;
    }

    @NonNull
    public Map<String, Product> toMap()
    {
        return _collection;
    }

    public boolean containsProduct(@NonNull final String productId)
    {
        return _collection.containsKey(productId);
    }
}
