package com.playgendary.amazon.iap.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * DAO class for sample purchase data
 * 
 * 
 */
public class EntitlementsDataSource {

    private static final String TAG = EntitlementsDataSource.class.getName();
    private final String[] ALL_COLUMNS = {
            DbContext.COLUMN_RECEIPT_ID,
            DbContext.COLUMN_USER_ID,
            DbContext.COLUMN_PRODUCT_ID,
            DbContext.COLUMN_PURCHASE_DATE,
            DbContext.COLUMN_CANCEL_DATE
    };

    private final DbContext _dbContext;

    public EntitlementsDataSource(@NonNull final DbContext dbContext)
    {
        _dbContext = dbContext;
    }

    private EntitlementRecord cursorToEntitlementRecord(final Cursor cursor)
    {
        final EntitlementRecord entitlementRecord = new EntitlementRecord();
        entitlementRecord.setReceiptId(cursor.getString(cursor.getColumnIndex(DbContext.COLUMN_RECEIPT_ID)));
        entitlementRecord.setUserId(cursor.getString(cursor.getColumnIndex(DbContext.COLUMN_USER_ID)));
        entitlementRecord.setProductId(cursor.getString(cursor.getColumnIndex(DbContext.COLUMN_PRODUCT_ID)));
        entitlementRecord.setPurchaseDate(cursor.getLong(cursor.getColumnIndex(DbContext.COLUMN_PURCHASE_DATE)));
        entitlementRecord.setCancelDate(cursor.getLong(cursor.getColumnIndex(DbContext.COLUMN_CANCEL_DATE)));
        return entitlementRecord;
    }

    /**
     * Return the entitlement for given user and sku
     * 
     * @param userId
     * @param sku
     * @return
     */
    public final EntitlementRecord getLatestEntitlementRecordBySku(final String userId, final String sku)
    {
        Log.d(TAG, "getEntitlementRecordBySku: userId (" + userId + "), sku (" + sku + ")");

        final SQLiteDatabase database = _dbContext.getWritableDatabase();
        final String where = DbContext.COLUMN_USER_ID + " = ? and " + DbContext.COLUMN_PRODUCT_ID + " = ?";
        final Cursor cursor = database.query(
                DbContext.TABLE_ENTITLEMENTS,
                ALL_COLUMNS,
                where,
                new String[] { userId, sku },
                null,
                null,
                DbContext.COLUMN_PURCHASE_DATE + " desc ");

        final EntitlementRecord result;
        cursor.moveToFirst();
        if (cursor.isAfterLast())
        {
            result = null;
            Log.d(TAG, "getEntitlementRecordBySku: no record found ");
        }
        else
        {
            result = cursorToEntitlementRecord(cursor);
            Log.d(TAG, "getEntitlementRecordBySku: found ");
        }
        cursor.close();
        return result;
    }

    /**
     * Insert or update the entitlement records table with specified receipt id as primary key.
     *  
     * @param receiptId
     * @param userId
     * @param sku
     * @param purchaseDate
     * @param cancelDate
     */
    public void insertOrUpdateEntitlementRecord(
            final String receiptId,
            final String userId,
            final String sku,
            final long purchaseDate,
            final long cancelDate)
    {
        Log.d(TAG, "insertOrUpdateEntitlementRecord: receiptId (" + receiptId + "),userId (" + userId + ")");
        final String where = DbContext.COLUMN_RECEIPT_ID + " = ? and "
                             + DbContext.COLUMN_CANCEL_DATE
                             + " > 0";

        final SQLiteDatabase database = _dbContext.getWritableDatabase();
        final Cursor cursor = database.query(
                DbContext.TABLE_ENTITLEMENTS,
                ALL_COLUMNS,
                where,
                new String[] { receiptId },
                null,
                null,
                null);

        final int count = cursor.getCount();
        cursor.close();
        if (count > 0)
        {
            // There are record with given receipt id and cancel_date>0 in the
            // table, this record should be final and cannot be overwritten
            // anymore.
            Log.w(TAG, "Record already in final state");
        }
        else
        {
            // Insert the record into database with CONFLICT_REPLACE flag.
            final ContentValues values = new ContentValues();
            values.put(DbContext.COLUMN_RECEIPT_ID, receiptId);
            values.put(DbContext.COLUMN_USER_ID, userId);
            values.put(DbContext.COLUMN_PRODUCT_ID, sku);
            values.put(DbContext.COLUMN_PURCHASE_DATE, purchaseDate);
            values.put(DbContext.COLUMN_CANCEL_DATE, cancelDate);
            database.insertWithOnConflict(DbContext.TABLE_ENTITLEMENTS,
                                          null,
                                          values,
                                          SQLiteDatabase.CONFLICT_REPLACE);
        }
    }

    /**
     * Find entitlement record by specified receipt ID
     * @param userId
     * @param receiptId
     * @return
     */
    public EntitlementRecord getEntitlementRecordByReceiptId(final String receiptId)
    {
        Log.d(TAG, "getEntitlementRecordByReceiptId: receiptId (" + receiptId + ")");

        final SQLiteDatabase database = _dbContext.getWritableDatabase();
        final String where = DbContext.COLUMN_RECEIPT_ID + "= ?";
        final Cursor cursor = database.query(
                DbContext.TABLE_ENTITLEMENTS,
                ALL_COLUMNS,
                where,
                new String[] { receiptId },
                null,
                null,
                null);

        final EntitlementRecord result;
        cursor.moveToFirst();
        if (cursor.isAfterLast())
        {
            result = null;
            Log.d(TAG, "getEntitlementRecordByReceiptId: no record found ");
        }
        else
        {
            result = cursorToEntitlementRecord(cursor);
            Log.d(TAG, "getEntitlementRecordByReceiptId: found ");
        }
        cursor.close();
        return result;
    }

    /**
     * Return all subscription records for the user
     *
     * @param userId
     *            user id used to verify the purchase record
     * @return
     */
    public final List<EntitlementRecord> getEntitlementRecordsByUserId(@NonNull final String userId, boolean onlyActive)
    {
        Log.d(TAG, "getEntitlementRecordsByUserId: userId (" + userId + ")");

        final SQLiteDatabase database = _dbContext.getWritableDatabase();
        String where;
        String[] selArgs;
        if (onlyActive)
        {
            where = DbContext.COLUMN_USER_ID + " = ? AND " + DbContext.COLUMN_CANCEL_DATE + " = ?";
            selArgs = new String[] { userId, String.valueOf(EntitlementRecord.DATE_NOT_SET) };
        }
        else
        {
            where = DbContext.COLUMN_USER_ID + " = ?";
            selArgs = new String[] { userId };
        }

        final Cursor cursor = database.query(DbContext.TABLE_ENTITLEMENTS,
                ALL_COLUMNS,
                where,
                selArgs,
                null,
                null,
                null);
        cursor.moveToFirst();

        final List<EntitlementRecord> results = new ArrayList<>();
        while (!cursor.isAfterLast()) {
            final EntitlementRecord rec = cursorToEntitlementRecord(cursor);
            results.add(rec);
            cursor.moveToNext();
        }

        Log.d(TAG, "getEntitlementRecordsByUserId: found " + results.size() + " records");
        cursor.close();
        return results;
    }

    /**
     * Cancel the specified Entitlement record by set the cancel date
     * @param receiptId
     * @param cancelDate
     * @return
     */
    public boolean cancelEntitlement(final String receiptId, final long cancelDate)
    {
        Log.d(TAG, "cancelEntitlement: receiptId (" + receiptId + "), cancelDate:(" + cancelDate + ")");

        final SQLiteDatabase database = _dbContext.getWritableDatabase();
        final String where = DbContext.COLUMN_RECEIPT_ID + " = ?";
        final ContentValues values = new ContentValues();
        values.put(DbContext.COLUMN_CANCEL_DATE, cancelDate);
        final int updated = database.update(
                DbContext.TABLE_ENTITLEMENTS,
                values,
                where,
                new String[] { receiptId });

        Log.d(TAG, "cancelEntitlement: updated " + updated);
        return updated > 0;
    }
}
