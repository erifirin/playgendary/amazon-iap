package com.playgendary.amazon.iap.data;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DbContext extends SQLiteOpenHelper
{
    private static final String TAG = DbContext.class.getName();

    public static final String TABLE_SUBSCRIPTIONS = "subscriptions";
    public static final String TABLE_CONSUMABLE = "consumables";
    public static final String TABLE_ENTITLEMENTS = "entitlements";

    public static final String COLUMN_PRODUCT_ID = "product_id";
    public static final String COLUMN_RECEIPT_ID = "receipt_id";
    public static final String COLUMN_USER_ID = "user_id";
    public static final String COLUMN_DATE_FROM = "date_from";
    public static final String COLUMN_DATE_TO = "date_to";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_PURCHASE_DATE = "purchase_date";
    public static final String COLUMN_CANCEL_DATE = "cancel_date";

    private static final String DATABASE_NAME = "InAppPurchase.db";
    private static final int DATABASE_VERSION = 1;

    // Subscriptions creation sql statement
    private static final String TABLE_SUBSCRIPTIONS_CREATE = "create table " + TABLE_SUBSCRIPTIONS
            + "("
            + COLUMN_RECEIPT_ID
            + " text primary key not null, "
            + COLUMN_USER_ID
            + " text not null, "
            + COLUMN_PRODUCT_ID
            + " text not null, "
            + COLUMN_DATE_FROM
            + " integer not null, "
            + COLUMN_DATE_TO
            + " integer"
            + ");";

    // Consumables creation sql statement
    private static final String TABLE_CONSUMABLE_CREATE = "create table " + TABLE_CONSUMABLE
            + "("
            + COLUMN_RECEIPT_ID
            + " text primary key not null, "
            + COLUMN_USER_ID
            + " text not null, "
            + COLUMN_STATUS
            + " text not null "
            + ");";

    // Entitlements creation sql statement
    private static final String TABLE_ENTITLEMENTS_CREATE = "create table " + TABLE_ENTITLEMENTS
            + "("
            + COLUMN_RECEIPT_ID
            + " text primary key not null, "
            + COLUMN_USER_ID
            + " text not null, "
            + COLUMN_PURCHASE_DATE
            + " integer, "
            + COLUMN_CANCEL_DATE
            + " integer, "
            + COLUMN_PRODUCT_ID
            + " text not null "
            + ");";

    private SQLiteDatabase _database = null;

    public DbContext(final Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public SQLiteDatabase getDatabase() throws SQLException
    {
        if (_database == null)
            _database = getWritableDatabase();
        return _database;
    }

    @Override
    public void close()
    {
        if (_database != null)
        {
            _database.close();
            _database = null;
        }

        super.close();
    }

    @Override
    public void onCreate(final SQLiteDatabase database)
    {
        database.execSQL(TABLE_SUBSCRIPTIONS_CREATE);
        database.execSQL(TABLE_CONSUMABLE_CREATE);
        database.execSQL(TABLE_ENTITLEMENTS_CREATE);
    }

    @Override
    public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion)
    {
        Log.w(TAG, "Upgrading database from version " + oldVersion + " to " + newVersion);

        // migration
    }
}
