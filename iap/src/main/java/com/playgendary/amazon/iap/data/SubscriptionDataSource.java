package com.playgendary.amazon.iap.data;

// based on Amazon Example Project

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class SubscriptionDataSource
{
    private static final String TAG = SubscriptionDataSource.class.getName();
    private static final String[] ALL_COLUMNS = {
            DbContext.COLUMN_RECEIPT_ID,
            DbContext.COLUMN_USER_ID,
            DbContext.COLUMN_PRODUCT_ID,
            DbContext.COLUMN_DATE_FROM,
            DbContext.COLUMN_DATE_TO
    };

    private final DbContext _dbContext;

    public SubscriptionDataSource(@NonNull final DbContext dbContext)
    {
        _dbContext = dbContext;
    }

    @NonNull
    private SubscriptionRecord cursorToSubscriptionRecord(@NonNull final Cursor cursor)
    {
        final SubscriptionRecord subsRecord = new SubscriptionRecord();
        subsRecord.setReceiptId(cursor.getString(cursor.getColumnIndex(DbContext.COLUMN_RECEIPT_ID)));
        subsRecord.setUserId(cursor.getString(cursor.getColumnIndex(DbContext.COLUMN_USER_ID)));
        subsRecord.setProductId(cursor.getString(cursor.getColumnIndex(DbContext.COLUMN_PRODUCT_ID)));
        subsRecord.setFrom(cursor.getLong(cursor.getColumnIndex(DbContext.COLUMN_DATE_FROM)));
        subsRecord.setTo(cursor.getLong(cursor.getColumnIndex(DbContext.COLUMN_DATE_TO)));
        return subsRecord;
    }

    /**
     * Return all subscription records for the user
     *
     * @param userId
     *            user id used to verify the purchase record
     * @return
     */
    @NonNull
    public final List<SubscriptionRecord> getSubscriptionRecords(@NonNull final String userId, boolean onlyActive)
    {
        Log.d(TAG, "getSubscriptionRecord: userId (" + userId + ")");

        final SQLiteDatabase database = _dbContext.getWritableDatabase();
        String where;
        String[] selArgs;
        if (onlyActive)
        {
            where = DbContext.COLUMN_USER_ID + " = ? AND " + DbContext.COLUMN_DATE_TO + " = ?";
            selArgs = new String[] { userId, String.valueOf(SubscriptionRecord.DATE_NOT_SET) };
        }
        else
        {
            where = DbContext.COLUMN_USER_ID + " = ?";
            selArgs = new String[] { userId };
        }

        final Cursor cursor = database.query(DbContext.TABLE_SUBSCRIPTIONS,
                ALL_COLUMNS,
                where,
                selArgs,
                null,
                null,
                null);
        cursor.moveToFirst();

        final List<SubscriptionRecord> results = new ArrayList<SubscriptionRecord>();
        while (!cursor.isAfterLast()) {
            final SubscriptionRecord subsRecord = cursorToSubscriptionRecord(cursor);
            results.add(subsRecord);
            cursor.moveToNext();
        }
        Log.d(TAG, "getSubscriptionRecord: found " + results.size() + " records");
        cursor.close();
        return results;
    }

    @NonNull
    public final List<SubscriptionRecord> getSubscriptionRecordsByProductId(
            @NonNull final String userId,
            @NonNull final String productId,
            @NonNull boolean onlyActive)
    {
        Log.d(TAG, "getSubscriptionRecords: userId (" + userId + "), productId (" + productId + ")");

        final SQLiteDatabase database = _dbContext.getWritableDatabase();
        String where;
        String[] selArgs;
        if (onlyActive)
        {
            where = DbContext.COLUMN_USER_ID + " = ? AND " + DbContext.COLUMN_PRODUCT_ID + " = ? AND " + DbContext.COLUMN_DATE_TO + " = ?";
            selArgs = new String[] { userId, productId, String.valueOf(SubscriptionRecord.DATE_NOT_SET) };
        }
        else
        {
            where = DbContext.COLUMN_USER_ID + " = ? AND " + DbContext.COLUMN_PRODUCT_ID + " = ?";
            selArgs = new String[] { userId, productId };
        }

        final Cursor cursor = database.query(DbContext.TABLE_SUBSCRIPTIONS,
                ALL_COLUMNS,
                where,
                selArgs,
                null,
                null,
                null);
        cursor.moveToFirst();

        final List<SubscriptionRecord> results = new ArrayList<SubscriptionRecord>();
        while (!cursor.isAfterLast()) {
            final SubscriptionRecord subsRecord = cursorToSubscriptionRecord(cursor);
            results.add(subsRecord);
            cursor.moveToNext();
        }
        Log.d(TAG, "getSubscriptionRecord: found " + results.size() + " records");
        cursor.close();
        return results;
    }

    @Nullable
    public final SubscriptionRecord getSubscriptionRecordByReceiptId(
            @NonNull final String userId,
            @NonNull final String receiptId)
    {
        Log.d(TAG, "getSubscriptionRecord: userId (" + userId + "), receiptID (" + receiptId + ")");

        final SQLiteDatabase database = _dbContext.getWritableDatabase();
        final String where = DbContext.COLUMN_USER_ID + " = ? AND " + DbContext.COLUMN_RECEIPT_ID + " = ?";
        final Cursor cursor = database.query(DbContext.TABLE_SUBSCRIPTIONS,
                ALL_COLUMNS,
                where,
                new String[] { userId, receiptId },
                null,
                null,
                null);
        cursor.moveToFirst();

        SubscriptionRecord result = null;
        if (!cursor.isAfterLast())
            result = cursorToSubscriptionRecord(cursor);
        Log.d(TAG, "getSubscriptionRecord: found " + (result == null ? "0" : "1") + " records");
        cursor.close();
        return result;
    }

    /**
     * Insert or update the subscription record by receiptId
     *
     * @param receiptId
     *            The receipt id
     * @param userId
     *            Amazon user id
     * @param productId
     *            The sku
     * @param dateFrom
     *            Timestamp for subscription's valid from date
     * @param dateTo
     *            Timestamp for subscription's valid to date. less than 1 means
     *            cancel date not set, the subscription in active status.
     */
    public void insertOrUpdateSubscriptionRecord(
            @NonNull final String receiptId,
            @NonNull final String userId,
            @NonNull final String productId,
            final long dateFrom,
            final long dateTo)
    {
        Log.d(TAG, "insertOrUpdateSubscriptionRecord: receiptId (" + receiptId + "),userId (" + userId + ")");
        final String where = DbContext.COLUMN_RECEIPT_ID + " = ? and "
                + DbContext.COLUMN_DATE_TO
                + " > 0";

        final SQLiteDatabase database = _dbContext.getWritableDatabase();
        final Cursor cursor = database.query(DbContext.TABLE_SUBSCRIPTIONS,
                ALL_COLUMNS,
                where,
                new String[] { receiptId },
                null,
                null,
                null);

        final int count = cursor.getCount();
        cursor.close();
        if (count > 0)
        {
            // There are record with given receipt id and cancel_date>0 in the
            // table, this record should be final and cannot be overwritten
            // anymore.
            Log.w(TAG, "Record already in final state");
        }
        else
        {
            // Insert the record into database with CONFLICT_REPLACE flag.
            final ContentValues values = new ContentValues();
            values.put(DbContext.COLUMN_RECEIPT_ID, receiptId);
            values.put(DbContext.COLUMN_USER_ID, userId);
            values.put(DbContext.COLUMN_PRODUCT_ID, productId);
            values.put(DbContext.COLUMN_DATE_FROM, dateFrom);
            values.put(DbContext.COLUMN_DATE_TO, dateTo);

            database.insertWithOnConflict(DbContext.TABLE_SUBSCRIPTIONS,
                    null,
                    values,
                    SQLiteDatabase.CONFLICT_REPLACE);
        }
    }

    /**
     * Cancel a subscription by set the cancel date for the subscription record
     *
     * @param receiptId
     *            The receipt id
     * @param cancelDate
     *            Timestamp for the cancel date
     * @return
     */
    public boolean cancelSubscription(@NonNull final String receiptId, final long cancelDate)
    {
        Log.d(TAG, "cancelSubscription: receiptId (" + receiptId + "), cancelDate:(" + cancelDate + ")");

        final SQLiteDatabase database = _dbContext.getWritableDatabase();
        final String where = DbContext.COLUMN_RECEIPT_ID + " = ?";
        final ContentValues values = new ContentValues();
        values.put(DbContext.COLUMN_DATE_TO, cancelDate);

        final int updated = database.update(DbContext.TABLE_SUBSCRIPTIONS,
                values,
                where,
                new String[] { receiptId });

        Log.d(TAG, "cancelSubscription: updated " + updated);
        return updated > 0;
    }
}
