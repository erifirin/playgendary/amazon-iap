package com.playgendary.amazon.iap;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.amazon.device.iap.model.RequestId;

import java.util.HashMap;
import java.util.Map;

class OperationsCollection
{
    private final Map<RequestId, OperationContext> _operations = new HashMap<>();
    private OperationContext _interlockedOperation = null;

    public void addAsyncOperation(@NonNull final OperationContext ctx)
    {
        _operations.put(ctx.getRequestId(), ctx);
    }

    @Nullable
    public OperationContext getAsyncOperation(@NonNull final RequestId requestId)
    {
        OperationContext ctx = _operations.get(requestId);
        if (ctx == null)
            Log.e(IapService.TAG, "Failed to find operation context with requestId: " + requestId.toString());

        return ctx;
    }

    @Nullable
    public OperationContext removeAsyncOperation(@NonNull final RequestId requestId)
    {
        OperationContext ctx = _operations.remove(requestId);
        if (ctx == null)
            Log.e(IapService.TAG, "Failed to find operation context with requestId: " + requestId.toString());

        return ctx;
    }

    public boolean setInterlockedOperation(@NonNull final OperationContext ctx)
    {
        if (_interlockedOperation != null && _interlockedOperation != ctx)
            return false;

        _interlockedOperation = ctx;
        return true;
    }

    @Nullable
    public OperationContext peekInterlockedOperation(@NonNull final RequestId requestId)
    {
        if (_interlockedOperation == null || !requestId.equals(_interlockedOperation.getRequestId()))
            return null;

        return _interlockedOperation;
    }

    public boolean releaseInterlockedOperation(@NonNull final OperationContext ctx)
    {
        if (_interlockedOperation != ctx)
            return false;

        _interlockedOperation = null;
        return true;
    }

    @Nullable
    public OperationContext releaseInterlockedOperation(@NonNull final RequestId requestId)
    {
        if (_interlockedOperation == null || !requestId.equals(_interlockedOperation.getRequestId()))
            return null;

        OperationContext ctx = _interlockedOperation;
        _interlockedOperation = null;
        return ctx;
    }
}

