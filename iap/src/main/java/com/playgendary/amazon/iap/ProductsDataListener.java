package com.playgendary.amazon.iap;

import android.support.annotation.NonNull;

public interface ProductsDataListener
{
    void onProductDataResponse(@NonNull ProductsDataOperationContext context);
}

