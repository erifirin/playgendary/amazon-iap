package com.playgendary.amazon.iap.data;

// based in Amazon Example Project

public class SubscriptionRecord
{
    public static final int DATE_NOT_SET = -1;

    private String _receiptId;
    private String _userId;
    private String _productId;
    private long _from;
    private long _to = DATE_NOT_SET;

    public long getFrom()
    {
        return _from;
    }

    public void setFrom(final long subscriptionFrom)
    {
        _from = subscriptionFrom;
    }

    public long getTo()
    {
        return _to;
    }

    public void setTo(final long subscriptionTo)
    {
        _to = subscriptionTo;
    }

    public boolean isActiveNow()
    {
        return _to == DATE_NOT_SET;
    }

    public boolean isActiveForDate(final long date)
    {
        return date >= _from && (isActiveNow() || date <= _to);
    }

    public String getReceiptId()
    {
        return _receiptId;
    }

    public void setReceiptId(final String receiptId)
    {
        _receiptId = receiptId;
    }

    public String getUserId()
    {
        return _userId;
    }

    public void setUserId(final String userId)
    {
        _userId = userId;
    }

    public String getProductId()
    {
        return _productId;
    }

    public void setProductId(final String productId)
    {
        _productId = productId;
    }
}
