package com.playgendary.amazon.iap;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.amazon.device.iap.model.Product;
import com.amazon.device.iap.model.ProductDataResponse;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class ProductsDataOperationContext extends OperationContext
{
    private static final int PAGE_SIZE = 80;
    private final @NonNull Set<String> _productIds;
    private @Nullable Iterator<String> _productIdsIterator = null;
    private @Nullable ProductsDataListener _callback;
    private @Nullable ProductDataResponse _response;
    private @NonNull Map<String, Product> _productsData = new HashMap<>();

    ProductsDataOperationContext(@NonNull Set<String> productIds, @Nullable final ProductsDataListener callback)
    {
        _productIds = productIds;
        _callback = callback;
    }

    @Nullable
    public ProductDataResponse.RequestStatus getRequestStatus()
    {
        return _response != null ? _response.getRequestStatus() : null;
    }

    @Nullable
    public Map<String, Product> getProductsData()
    {
        return _productsData;
    }

    void addProductsData(@Nullable Map<String, Product> productData)
    {
        if (productData != null)
            _productsData.putAll(productData);
    }

    @Nullable
    public Set<String> getUnavailableProductIds()
    {
        return _response != null ? _response.getUnavailableSkus() : null;
    }


    @Nullable
    Set<String> getNextProductsIdsPage()
    {
        if (_productIdsIterator == null)
            _productIdsIterator = _productIds.iterator();

        if (!_productIdsIterator.hasNext())
            return null;

        Set<String> ids = new HashSet<>(PAGE_SIZE);
        int i = 0;
        while (i < PAGE_SIZE && _productIdsIterator.hasNext())
        {
            ids.add(_productIdsIterator.next());
            i++;
        }

        return ids;
    }

    void invokeCallback(int resultCode, @NonNull final ProductDataResponse response)
    {
        setResultCode(resultCode);
        _response = response;

        if (_callback == null)
            return;

        ProductsDataListener callback = _callback;
        _callback = null;
        callback.onProductDataResponse(this);
    }

    @NonNull
    @Override
    public String toString()
    {
        ProductDataResponse.RequestStatus requestStatus = getRequestStatus();

        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append(", purchaseUpdatesRequestStatus: ").append(requestStatus != null ? requestStatus.toString() : "null");

        Map<String, Product> products = getProductsData();
        if (products != null)
            for (String id : products.keySet())
            {
                Product p = products.get(id);
                if (p != null)
                    sb.append(p.toString()).append('\n');
            }

        return sb.toString();
    }
}
