package com.playgendary.amazon.iap;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.amazon.device.iap.model.ProductType;
import com.amazon.device.iap.model.PurchaseResponse;
import com.amazon.device.iap.model.Receipt;

import java.util.Locale;

public class PurchaseProductOperationContext extends OperationContext
{
    private @Nullable PurchaseProductListener _callback;
    private @NonNull final String _productId;
    private Receipt _receipt;
    private PurchaseResponse.RequestStatus _requestStatus;


    PurchaseProductOperationContext(@NonNull final String productId, @Nullable final PurchaseProductListener callback)
    {
        _productId = productId;
        _callback = callback;
    }

    void setPurchaseResponse(@NonNull final PurchaseResponse response)
    {
        setUserData(response.getUserData());
        _receipt = response.getReceipt();
        _requestStatus = response.getRequestStatus();
    }

    @Nullable
    public Receipt getReceipt()
    {
        return _receipt;
    }

    @NonNull
    public String getProductId()
    {
        return _productId;
    }

    @Nullable
    public ProductType getProductType()
    {
        if (_receipt != null)
            return _receipt.getProductType();
        return null;
    }

    public boolean isAlreadyOwn()
    {
        return _requestStatus != null && _requestStatus == PurchaseResponse.RequestStatus.ALREADY_PURCHASED;
    }

    void invokeCallback(int resultCode)
    {
        invokeCallback(resultCode, null);
    }

    void invokeCallback(int resultCode, @Nullable final String resultMessage)
    {
        setResultCode(resultCode);
        setResultMessage(resultMessage);

        if (_callback != null)
        {
            PurchaseProductListener callback = _callback;
            _callback = null;
            callback.onPurchaseResponse(this);
        }
    }

    @NonNull
    @Override
    public String toString()
    {
        return String.format(Locale.getDefault(), "%s, purchaseRequestStatus: \"%s\", receipt: %s",
                super.toString(),
                _requestStatus != null ? _requestStatus.toString() : "null",
                _receipt != null ? _receipt.toString() : "null");
    }
}
