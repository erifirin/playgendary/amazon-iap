package com.playgendary.amazon.iap.data;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class ConsumableRecord
{
    private ConsumableDataSource.PurchaseStatus _status;
    private String _receiptId;
    private String _userId;

    public ConsumableDataSource.PurchaseStatus getStatus()
    {
        return _status;
    }

    public void setStatus(final ConsumableDataSource.PurchaseStatus status)
    {
        _status = status;
    }

    public @Nullable String getReceiptId()
    {
        return _receiptId;
    }

    public void setReceiptId(@NonNull final String receiptId)
    {
        _receiptId = receiptId;
    }

    public @Nullable String getUserId()
    {
        return _userId;
    }

    public void setUserId(@NonNull final String userId)
    {
        _userId = userId;
    }

}